//
//  AppDelegate.swift
//  Transform
//
//  Created by Yin on 07/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import Firebase
import UserNotifications
import EBBannerView
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {

    var window: UIWindow?

    let gcmMessageIDKey = "gcm.message_id"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        /// setup firebase messaging
        FirebaseApp.configure()

        // [START set_messaging_delegate]       
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        initMenu()

        // Initialize sign-in
       
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self

        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        //return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        var _: [String: AnyObject] = [UIApplicationOpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,
                                            UIApplicationOpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
        return GIDSignIn.sharedInstance().handle(url,
                                                    sourceApplication: sourceApplication,
                                                    annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        FBSDKAppEvents.activateApp()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0
    }
    
    /// GIDSignInDelegate protocol
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            // TODO:
            print(error)
            return
        }
        
        guard let authentication = user.authentication else {return}
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        print(credential)
        //TODO: todo
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // perform any operations when the user disconnects from app here.
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
        
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the InstanceID token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    func registerToken() {
        
        if currentUser?.userId == 0 {
            return
        }
       
        if let token = UserDefaults.standard.string(forKey: Const.KEY_TOKEN) {
            
            ApiRequest.saveToken(token: token, completion: { (rescode) in
                
            })
        }
    }
    
    /// custom function
    
    func initMenu() {
        
        let storyboard = UIStoryboard.init(name: "MainNew", bundle: nil)
        let leftMenu = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let rightMenu = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        SlideNavigationController.sharedInstance().rightMenu = rightMenu
        SlideNavigationController.sharedInstance().leftMenu = leftMenu
        SlideNavigationController.sharedInstance().menuRevealAnimationDuration = 0.2
        SlideNavigationController.sharedInstance().panGestureSideOffset = 0
        SlideNavigationController.sharedInstance().enableSwipeGesture = false
        SlideNavigationController.sharedInstance().enableShadow = false
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidClose), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Closed", menu)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidOpen), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Opened", menu)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(SlideNavigationControllerDidReveal), object: nil, queue: nil) { (notification) in
            let menu = notification.userInfo!["menu"]!
            print("Revealed", menu)
        }
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let body = notification.request.content.body
        let userInfo = notification.request.content.userInfo
        let json = JSON(userInfo)
        let type = json[Const.PARAM_MSGTYPE].stringValue
        let groupId = json[Const.PARAM_GROUPID].intValue
        let content = json[Const.PARAM_CONTENT]
  
        var messageModel = MessageModel()
        if let dataFromString = content.stringValue.data(using: .utf8, allowLossyConversion: false) {
            do{
                let json = try JSON(data: dataFromString)
                messageModel = ParseHelper.parseMessage(json)
            }
            catch {
            }
        }
        
        let banner = EBBannerView.banner({ (make) in
            make?.style = EBBannerViewStyle(rawValue: 11)
            make?.icon = UIImage.init(named: "AppIcon")
            make?.title = Const.APP_NAME
            make?.content = body
            make?.date = "Now"
        })

        if type == Const.KEY_PUSH_CHAT {
            
            if let vc = g_currentVC as? ChattingViewController {
                if vc._group.id == groupId && messageModel.userId != Int(vc.senderId) {
                    vc.onReceiveMessage(message: messageModel)
                }
            } else {
                banner?.show()
            }
        } else if type == Const.KEY_PUSH_BENCHMARK {
            
            if let vc = g_currentVC as? HomeViewController {
                vc.addPoint(point: 5)
            }
         
            banner?.show()
        } else {
            banner?.show()
        }
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        
        // Print full message.
        print(userInfo)
        completionHandler()
    }
}

// [END ios_10_message_handling]
// [START ios_10_data_message_handling]

extension AppDelegate: MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
       
        UserDefaults.standard.set(fcmToken, forKey: Const.KEY_TOKEN)
        registerToken()
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}


