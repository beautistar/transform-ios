//
//  MessageModel.swift
//  Transform
//
//  Created by Yin on 21/12/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation

class MessageModel {
    
    var messageId: Int = 0
    var userId: Int = 0
    var userName = ""
    var photoUrl = ""
    var messageContent = ""    
}
