//
//  RankingModel.swift
//  Transform
//
//  Created by Yin on 03/01/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation


class RankingModel {
    
    var userId: Int = 0
    var userName: String = ""
    var photoUrl: String = ""
    var totalPoints: Int = 0
    var benchmark: Int = 0
}
