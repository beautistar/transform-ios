//
//  GroupModel.swift
//  Transform
//
//  Created by Yin on 30/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation

class GroupModel {
    
    var id = 0
    var creatorId = 0
    var groupName = ""
    var kindOfGroup = ""    // private or public
    var groupType = ""      // Macro or general
    var pointParams = ""
    var price : Float = 0.00
    var creatorName = ""
    var startDate = ""
    var endDate = ""
    var challengeFinished = 0 // 0: unfinished, 1: finished
}

