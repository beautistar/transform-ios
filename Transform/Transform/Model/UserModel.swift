//
//  UserModel.swift
//  Transform
//
//  Created by Yin on 24/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class UserModel {

    var userId: Int = 0
    var userName = ""
    var email = ""
    var password = ""
    var property = PropertyModel()
    var groupStatusIn = ""
    var photoUrl = ""
    
    var macro = MacroModel()
    var points = [PointModel]()
    
    var token: String {
        get {
            if let token = UserDefaults.standard.value(forKey: Const.KEY_TOKEN) {
                return token as! String
            }
            return ""
        }
    }
}
