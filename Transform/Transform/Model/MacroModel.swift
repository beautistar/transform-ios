//
//  MacroModel.swift
//  Transform
//
//  Created by Yin on 30/12/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation

class MacroModel {
    
    var bodyFat: Float = 0
    var bodyMass: Float = 0
    var dailyProtein: Float = 0
    var dailyCarb: Float = 0
    var dailyFat: Float = 0
    var totalBlocks: Int = 0
    var eatBlocksTillNow: Int = 0
    var startingBodyFatPercent: Float = 0
    
}
