//
//  LibraryModel.swift
//  Transform
//
//  Created by Yin on 2018/5/20.
//  Copyright © 2018 Yin. All rights reserved.
//

import Foundation


class LibraryModel {
    
    var id: Int = 0
    var title: String = ""
    var description: String = ""
    var preview_picture: String = ""
    var file_url: String = ""
    var created_at: String = ""
    
}
