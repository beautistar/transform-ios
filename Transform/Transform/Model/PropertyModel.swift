//
//  PropertyModel.swift
//  Transform
//
//  Created by Yin on 26/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation

class PropertyModel {
    
    var gender = ""
    var birthday = ""
    var age: Int = 0
    var bodyWeight: Float = 0.00
    var height:Float = 0.00
    var activityFactor = ""
    var wristMeasurement: Float = 0.00
    var waistMeasurement: Float = 0.00
    var averageHip: Float = 0.00
    var averageAbdomen: Float = 0.00
    var startingBodyWeight: Float = 0.00

}
