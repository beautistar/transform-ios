//
//  LibraryCell.swift
//  Transform
//
//  Created by Yin on 2018/5/20.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class LibraryCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imvPicture: UIImageView!
    @IBOutlet weak var txvDescription: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
