//
//  RankingListCell.swift
//  Transform
//
//  Created by Yin on 04/01/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class RankingListCell: UITableViewCell {

    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblRanking: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var imvCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        

    }

}
