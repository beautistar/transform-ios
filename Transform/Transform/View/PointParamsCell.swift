//
//  PointParamsCell.swift
//  Transform
//
//  Created by Yin on 06/01/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class PointParamsCell: UITableViewCell {

    @IBOutlet weak var imvCheck: UIImageView!
    @IBOutlet weak var lblParam: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imvCheck.image = #imageLiteral(resourceName: "bg_checkbox_unchecked")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(status: Bool) {
        if status {
            imvCheck.image = #imageLiteral(resourceName: "bg_checkbox_checked")
        }
        else {
            imvCheck.image = #imageLiteral(resourceName: "bg_checkbox_unchecked")
        }
    }

}
