//
//  publicGroupCell.swift
//  Transform
//
//  Created by Yin on 15/12/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class PublicGroupCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
}
