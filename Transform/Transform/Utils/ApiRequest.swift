//
//  ApiRequest.swift
//  Transform
//
//  Created by Yin on 24/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftyUserDefaults
import JSQMessagesViewController

class ApiRequest {
    
    static let BASE_URL = "http://18.216.154.87/"
    static let SERVER_URL = BASE_URL + "index.php/api/"    
    
    static let REQ_REGISTER                 = SERVER_URL + "signup"
    static let REQ_LOGIN                    = SERVER_URL + "login"
    static let REQ_LOGIN_SOCIAL             = SERVER_URL + "loginWithSocial"
    static let REQ_UPLOAD_PERSONALIFO       = SERVER_URL + "uploadPersonalInfo"
    static let REQ_CREATE_GROUP             = SERVER_URL + "createGroup"
    static let REQ_ADDMEMBER                = SERVER_URL + "addMember"
    static let REQ_GETJOINGROUPINFO         = SERVER_URL + "getJoinGroupInfo"
    static let REQ_GETUSERLISTINGROUP       = SERVER_URL + "getUserListInGroup"
    static let REQ_JOINGROUP                = SERVER_URL + "joinGroup"
    static let REQ_GETALLGROUP              = SERVER_URL + "getAllGroupList"
    static let REQ_GETMESSAGES              = SERVER_URL + "getMessages"
    static let REQ_SENDMESSAGE              = SERVER_URL + "sendMessage"
    static let REQ_SAVE_TOKEN               = SERVER_URL + "saveToken"
    static let REQ_UPLOAD_PHOTO             = SERVER_URL + "uploadPhoto"
    static let REQ_GETRANKINGLIST           = SERVER_URL + "getRankingListInGroup"
    static let REQ_UPDATEMACROPOINT         = SERVER_URL + "updateMacroPoint"
    static let REQ_ADD_POINT                = SERVER_URL + "addPoint"
    static let REQ_DELETE_POINT             = SERVER_URL + "deletePoint"
    static let REQ_GETEATENBLOCK            = SERVER_URL + "getEatBlocksTillNow"
    static let REQ_SAETEATENBLOCK           = SERVER_URL + "saveEatBlocksTillNow"
    static let REQ_GETUSERBYNAME            = SERVER_URL + "getUserInfoByName"
    static let REQ_GIVEBEANCHMARK           = SERVER_URL + "giveBenchmark"
    static let REQ_PAYMENTPROCESS           = SERVER_URL + "paymentProcess"
    static let REQ_GETLIBRARY               = SERVER_URL + "getLibrary"
    static let REQ_GETHELP                  = SERVER_URL + "getHelp"
    static let REQ_BLOCKMESSAGE             = SERVER_URL + "blockMessage"
    static let REQ_GETPREVCHALLENGE         = SERVER_URL + "getPreviousChallenges"

    
    static func register(_ user: UserModel, completion: @escaping (Int, Int) -> ()) {
        
        let params = [Const.PARAM_USERNAME: user.userName,
                      Const.PARAM_EMAIL: user.email,
                      Const.PARAM_PASSWORD: user.password] as [String : Any]
        
        Alamofire.request(REQ_REGISTER, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                
                let dict = JSON(response.result.value!)
                let resultCode = dict[Const.RESULT_CODE].intValue
                if resultCode == Const.CODE_SUCCESS {
                     let userId = dict[Const.PARAM_USERID].intValue
                    /// working
                    
                    currentUser = user
                    
                    UserDefaults.standard.set(user.email, forKey: Const.KEY_EMAIL)
                    UserDefaults.standard.set(user.password, forKey: Const.KEY_PASSWORD)
                    
                    //=====
                    completion(resultCode, userId)
                } else if resultCode == Const.CODE_ALREADY_REGISTERED {
                    completion(resultCode, Const.CODE_0)
                }
                break
            case .failure(let error):
                completion(Const.CODE_FAIL, Const.CODE_0)
                print(error)
            }
        }
    }
    
    static func login(email: String, password: String, completion: @escaping (String) -> () ) {
        
        Alamofire.request(REQ_LOGIN, method: .post, parameters: [Const.PARAM_EMAIL : email, Const.PARAM_PASSWORD : password]).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.ERROR_CONNECT)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    
                    currentUser = ParseHelper.parseUserWithProperty(json)
                   
                    UserDefaults.standard.set(email, forKey: Const.KEY_EMAIL)
                    UserDefaults.standard.set(password, forKey: Const.KEY_PASSWORD)

                    completion(Const.RESULT_SUCCESS)
                    
                }
                else if resCode == Const.CODE_UNREGISTERED_USER {
                    completion(Const.UNREGISTERED_USER)
                }
                else if resCode == Const.CODE_INVALIDE_EMAIL_PWD {
                    completion(Const.WRONG_PASSWORD)
                }
            }
        }
    }
    
    static func loginSocial(username: String, email: String, password: String, completion: @escaping(Int) -> () ) {
        
        let params = [Const.PARAM_USERNAME: username, Const.PARAM_EMAIL : email, Const.PARAM_PASSWORD : password]
        
        Alamofire.request(REQ_LOGIN_SOCIAL, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isFailure{
                completion(Const.CODE_FAIL)
            }
            else
            {
                let json = JSON(response.result.value!)
                
                let resCode = json[Const.RESULT_CODE].intValue
                
                if resCode == Const.CODE_SUCCESS {
                    
                    let user = ParseHelper.parseUserWithProperty(json)
                    currentUser = user
                    
                    completion(Const.CODE_SUCCESS)
                    
                }   else {
                    
                    let user = UserModel()
                    user.userId = json[Const.PARAM_USERID].intValue
                    user.email = email
                    user.password = password
                    user.userName = username
                    
                    currentUser = user
                    
                    completion(Const.CODE_FIRST_SOCIAL_LOGIN)
                }
            }
        }
    }
    
    static func uploadProperty(userId: Int, property: PropertyModel, completion: @escaping (Int) -> ()) {
        
        let params = [Const.PARAM_ID: userId,
                      Const.PARAM_GENDER: property.gender,
                      Const.PARAM_BIRTHDAY: property.birthday,
                      Const.PARAM_AGE: property.age,
                      Const.PARAM_BODYWEIGHT: property.bodyWeight,
                      Const.PARAM_HEIGHT: property.height,
                      Const.PARAM_ACTIVITYFACTOR: property.activityFactor,
                      Const.PARAM_WRIST_MEASUREMENT: property.wristMeasurement,
                      Const.PARAM_WAIST_MEASUREMENT: property.waistMeasurement,
                      Const.PARAM_AVERAGEHIP: property.averageHip,
                      Const.PARAM_AVERAGEABDOMEN: property.averageAbdomen,
                      Const.PARAM_STARTINGBODYWEIGHT: property.startingBodyWeight] as [String : Any]
        
        Alamofire.request(REQ_UPLOAD_PERSONALIFO, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                    completion(Const.CODE_SUCCESS)
                break
            case .failure(let error):
                print(error)
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func createGroup(userId: Int, group: GroupModel, completion: @escaping(Int, Int) -> ()) {
    
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_GROUPNAME: group.groupName,
                      Const.PARAM_GROUPKIND: group.kindOfGroup,
                      Const.PARAM_GROUPTYPE: group.groupType,
                      Const.PARAM_POINTPARAMS: group.pointParams,
                      Const.PARAM_PRICE: group.price,
                      Const.PARAM_CREATORNAME: group.creatorName,
                      Const.PARAM_STARTDATE: group.startDate,
                      Const.PARAM_ENDDATE: group.endDate] as [String : Any]
        
        Alamofire.request(REQ_CREATE_GROUP, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            
            case .success:
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if resCode == Const.CODE_SUCCESS {
                    completion(Const.CODE_SUCCESS, json[Const.PARAM_GROUPID].intValue)
                } else {
                    completion(Const.CODE_GROUP_EXIST, json[Const.PARAM_GROUPID].intValue)
                }
                
            case .failure(let error) :
                print(error)
                completion(Const.CODE_FAIL, Const.CODE_0)
            }
        }
    }
    
    static func addMember(userId: Int, creatorName: String, email: String, groupId: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_CREATORNAME: creatorName,
                      Const.PARAM_EMAIL: email,
                      Const.PARAM_GROUPID: groupId] as [String : Any]
        
        Alamofire.request(REQ_ADDMEMBER, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
                
            case .success:                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                completion(resCode)
                
            case .failure(let error) :
                print(error)
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func getJoinGroupInfo(userId: Int, completion: @escaping(Int, [GroupModel], [GroupModel]) -> ()) {
        
        let params = [Const.PARAM_USERID: userId] as [String : Any]
        
        Alamofire.request(REQ_GETJOINGROUPINFO, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
           
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var publicGroups = [GroupModel]()
                    for groupObjc in json[Const.PARAM_PUBLICGROUP].arrayValue {
                        publicGroups.append(ParseHelper.parseGROUP(groupObjc))
                    }
                    var invitedGroups = [GroupModel]()
                    for inviteGroupObj in json[Const.PARAM_INVITEDGROUP].arrayValue {
                        invitedGroups.append(ParseHelper.parseGROUP(inviteGroupObj))
                    }
                    completion(Const.CODE_SUCCESS, invitedGroups, publicGroups)
                }
            } else {
                completion(Const.CODE_FAIL, [], [])
            }
        }
    }
    
    static func getUserListInGroup(groupId: Int, completion: @escaping(Int, [UserModel]) -> ()) {
        
        let params = [Const.PARAM_GROUPID: groupId] as [String : Any]
        
        Alamofire.request(REQ_GETUSERLISTINGROUP, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var userLists = [UserModel]()
                    for userObj in json[Const.PARAM_USERLIST].arrayValue {
                        userLists.append(ParseHelper.parseGroupUser(userObj))
                    }
                    
                    completion(Const.CODE_SUCCESS, userLists)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func joinGroup(userId: Int, groupId: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_GROUPID: groupId,
                      Const.PARAM_USERID: userId] as [String : Any]
        
        Alamofire.request(REQ_JOINGROUP, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    completion(Const.CODE_SUCCESS)
                }
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func getAllGroupList(userId: Int, completion: @escaping(Int, [GroupModel]) -> ()) {
        
        let params = [Const.PARAM_USERID: userId] as [String : Any]
        
        Alamofire.request(REQ_GETALLGROUP, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var groups = [GroupModel]()
                    for groupObjc in json[Const.PARAM_GROUPLIST].arrayValue {
                        groups.append(ParseHelper.parseGROUP(groupObjc))
                    }
                    completion(Const.CODE_SUCCESS, groups)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func getMessages(groupId: Int, completion: @escaping(Int, [MessageModel], [JSQMessage]) -> ()) {
        
        let params = [Const.PARAM_GROUPID: groupId,
                      Const.PARAM_READCOUNT: 0] as [String : Any]
        
        Alamofire.request(REQ_GETMESSAGES, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var messages = [MessageModel]()
                    var jsqMessages = [JSQMessage]()
                    for msgObj in json[Const.PARAM_CHATLIST].arrayValue {
                        
                        let msg = ParseHelper.parseMessage(msgObj)
                        messages.append(msg)
                        
                        let newMessage : JSQMessage!
                        
                        newMessage = JSQMessage(senderId: "\(msg.userId)", displayName: msg.userName,  text: msg.messageContent.decodeEmoji);
                        jsqMessages.append(newMessage)
                    }
                    completion(Const.CODE_SUCCESS, messages, jsqMessages)
                }
            } else {
                completion(Const.CODE_FAIL, [], [])
            }
        }
    }
    
    static func sendMessages(userId: Int, groupId: Int, message: String, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_GROUPID: groupId,
                      Const.PARAM_MSGCONTENT: message] as [String : Any]
        
        Alamofire.request(REQ_SENDMESSAGE, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                completion(Const.CODE_SUCCESS)
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func saveToken(token: String, completion: @escaping(Int) -> ()) {
        if let user = currentUser {
            let params = [Const.PARAM_USERID: user.userId,
                          Const.PARAM_TOKEN: token] as [String: Any]
            
            Alamofire.request(REQ_SAVE_TOKEN, method: .post, parameters: params).responseJSON { response in
                
                print(response)
                
                if response.result.isSuccess {
                    completion(Const.CODE_SUCCESS)
                } else {
                    completion(Const.CODE_FAIL)
                }
            }
        }
    }
    
    static func uploadPhoto(_ imgURL: String, userId: Int, completion: @escaping (Int,  String) -> ()) {
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath: imgURL), withName: "file")
                multipartFormData.append(String(userId).data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: Const.PARAM_USERID)
        },
            to: REQ_UPLOAD_PHOTO,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        switch response.result {
                            
                        case .success(_):
                            let json = JSON(response.result.value!)
                            let resCode = json[Const.RESULT_CODE].intValue
                            if (resCode == Const.CODE_SUCCESS) {
                                let imageurl = json[Const.PARAM_PHOTOURL].stringValue
                                completion(Const.CODE_SUCCESS, imageurl)
                            }
                            else {
                                completion(Const.CODE_FAIL, "")
                            }
                        case .failure(_):
                            
                            completion(Const.CODE_FAIL, "")
                        }
                    }
                    
                case .failure(_):
                    completion(Const.CODE_FAIL, "")
                }
        })
    }
    
    static func getRankingList(groupId: Int, completion: @escaping(Int, [RankingModel]) -> ()) {
        
        let params = [Const.PARAM_GROUPID: groupId] as [String : Any]
        
        Alamofire.request(REQ_GETRANKINGLIST, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var rankings = [RankingModel]()
                    
                    for rankingObj in json[Const.PARAM_RANKINGLIST].arrayValue {
                        rankings.append(ParseHelper.parseRanking(rankingObj))
                    }
                    completion(Const.CODE_SUCCESS, rankings)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func updateMacroPoint(userId: Int, macroOne: MacroModel, completion: @escaping (Int) -> ()) {
        
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_BODYFAT: macroOne.bodyFat,
                      Const.PARAM_BODYMASS: macroOne.bodyMass,
                      Const.PARAM_DAILYPROTEIN: macroOne.dailyProtein,
                      Const.PARAM_DAILYCARB: macroOne.dailyCarb,
                      Const.PARAM_DAILYFAT: macroOne.dailyFat,
                      Const.PARAM_TOTALBLOCKS: macroOne.totalBlocks,
                      Const.PARRAM_STARTBODYFATPER: macroOne.startingBodyFatPercent] as [String : Any]
        
        Alamofire.request(REQ_UPDATEMACROPOINT, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                completion(Const.CODE_SUCCESS)
                break
            case .failure(let error):
                print(error)
                completion(Const.CODE_FAIL)
                break
            }
        }
    }
    
    static func addPoint(userId: Int, groupId: Int, pointCnt: Int, completion: @escaping (Int) -> ()) {
        
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_GROUPID: groupId,
                      Const.PARAM_POINTCNT: pointCnt] as [String : Any]
        
        Alamofire.request(REQ_ADD_POINT, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                completion(Const.CODE_SUCCESS)
                break
            case .failure(let error):
                print(error)
                completion(Const.CODE_FAIL)
                break
            }
        }
    }
    
    static func deletePoint(userId: Int, groupId: Int, pointCnt: Int, completion: @escaping (Int) -> ()) {
        
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_GROUPID: groupId,
                      Const.PARAM_POINTCNT: pointCnt] as [String : Any]
        
        Alamofire.request(REQ_DELETE_POINT, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                completion(Const.CODE_SUCCESS)
                break
            case .failure(let error):
                print(error)
                completion(Const.CODE_FAIL)
                break
            }
        }
    }
    
    static func getEatBlockTillNow(userId: Int, completion: @escaping (Int, Int) -> ()) {
        
        let params = [Const.PARAM_USERID: userId] as [String : Any]
        
        Alamofire.request(REQ_GETEATENBLOCK, method: .post, parameters: params).responseJSON { response in
            
            print("getEatBlockTillNow", response)
            
            switch response.result {
            case .success:
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    let eatenBlock = json[Const.PARAM_EATBLOCKTILLNOW].intValue
                    completion(Const.CODE_SUCCESS, eatenBlock)                    
                } 
                break
            case .failure(let error):
                print(error)
                completion(Const.CODE_FAIL, Const.CODE_0)
                break
            }
        }
    }
    
    static func saveEatBlockTillNow(userId: Int, eatenBlock: Int, completion: @escaping (Int) -> ()) {
        
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_EATBLOCKTILLNOW: eatenBlock] as [String : Any]
        
        Alamofire.request(REQ_SAETEATENBLOCK, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                completion(Const.CODE_SUCCESS)
                break
            case .failure(let error):
                print(error)
                completion(Const.CODE_FAIL)
                break
            }
        }
    }
    
    static func getUserInfoByName(userName: String, completion: @escaping(Int, [UserModel]) -> ()) {
        
        let params = [Const.PARAM_USERNAME: userName] as [String: Any]
        
        Alamofire.request(REQ_GETUSERBYNAME, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    
                    var userList = [UserModel]()
                    
                    for userObj in json[Const.PARAM_USERMODELLIST].arrayValue {
                        let user = UserModel()
                        user.userId = userObj[Const.PARAM_USERID].intValue
                        user.email = userObj[Const.PARAM_EMAIL].stringValue
                        user.userName = userObj[Const.PARAM_USERNAME].stringValue
                        userList.append(user)
                    }
                    completion(Const.CODE_SUCCESS, userList)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func giveBenchmark(groupId: Int, userId: Int, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: userId,
                      Const.PARAM_GROUPID: groupId] as [String: Any]
        
        Alamofire.request(REQ_GIVEBEANCHMARK, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    
                    completion(Const.CODE_SUCCESS)
                }
            } else {
                completion(Const.CODE_FAIL)
            }
        }    
    }
    
    static func paymentProcess(groupId: Int, cardNumber: String, expirationDate: String, cardCode: String, amount: Float, completion: @escaping(Int) -> ()) {
        
        let params = [Const.PARAM_USERID: currentUser!.userId,
                      Const.PARAM_GROUPID: groupId,
                      Const.PARAM_USERNAME: currentUser!.userName,
                      Const.PARAM_EMAIL: currentUser!.email,
                      Const.PARAM_CARDNUMBER: cardNumber,
                      Const.PARAM_EXPIRATIONDATE: expirationDate,
                      Const.PARAM_CARDCODE: cardCode,
                      Const.PARAM_AMOUNT: amount
                      ] as [String: Any]
        
        Alamofire.request(REQ_PAYMENTPROCESS, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                    
                    completion(resCode)
                
            } else {
                completion(Const.CODE_FAIL)
            }
        }
    }
    
    static func getLibrary(completion: @escaping(Int, [LibraryModel]) -> ()) {
        
        Alamofire.request(REQ_GETLIBRARY, method: .post).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    
                    var libList = [LibraryModel]()
                    
                    for libObj in json[Const.PARAM_LIBRARY].arrayValue {
                        
                        let lib = ParseHelper.parseLibrary(libObj)
                        libList.append(lib)
                        
                    }
                    completion(Const.CODE_SUCCESS, libList)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func getHelp(completion: @escaping(Int, [LibraryModel]) -> ()) {
        
        Alamofire.request(REQ_GETHELP, method: .post).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    
                    var libList = [LibraryModel]()
                    
                    for libObj in json[Const.PARAM_HELP].arrayValue {
                        
                        let lib = ParseHelper.parseLibrary(libObj)
                        libList.append(lib)
                        
                    }
                    completion(Const.CODE_SUCCESS, libList)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func blockMessage(_ messageId: Int, messageContent: String, completion: @escaping (Int) -> ()) {
        
        let params = [Const.PARAM_MESSAGEID: messageId,
                      Const.PARAM_MSGCONTENT: messageContent] as [String : Any]
        
        Alamofire.request(REQ_BLOCKMESSAGE, method: .post, parameters: params).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                
                let dict = JSON(response.result.value!)
                let resultCode = dict[Const.RESULT_CODE].intValue
                if resultCode == Const.CODE_SUCCESS {
                    completion(resultCode)
                }
                break
            case .failure(let error):
                completion(Const.CODE_FAIL)
                print(error)
            }
        }
    }
    
    static func getPrevGroupList(userId: Int, completion: @escaping(Int, [GroupModel]) -> ()) {
        
        let params = [Const.PARAM_USERID: userId] as [String : Any]
        
        Alamofire.request(REQ_GETPREVCHALLENGE, method: .post, parameters: params).responseJSON { response in
            print("getPrevGroup")
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                let resCode = json[Const.RESULT_CODE].intValue
                if (resCode == Const.CODE_SUCCESS) {
                    var groups = [GroupModel]()
                    for groupObjc in json[Const.PARAM_GROUPLIST].arrayValue {
                        groups.append(ParseHelper.parseGROUP(groupObjc))
                    }
                    completion(Const.CODE_SUCCESS, groups)
                }
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
}
















