//
//  Calculation.swift
//  Transform
//
//  Created by Yin on 27/12/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation

class Calculation {
    
    
    //MARK: - Male calculation
    static func maleBodyFatPercent() -> Float {
        
        let path: String = Bundle.main.path(forResource: "table", ofType: "xlsx")!
        let spreadsheet: BRAOfficeDocumentPackage = BRAOfficeDocumentPackage.open(path)
        let maleWorkSheet: BRAWorksheet = spreadsheet.workbook.worksheets[0] as! BRAWorksheet
        
        var col = ""
        var row = ""
        
        let userX = currentUser!.property.waistMeasurement - currentUser!.property.wristMeasurement
        
        for index in 0...x_aixs.count - 1 {
            let xVal = 22 + (0.5 * Float(index))
            if userX == xVal {
                col = x_aixs[index+1]
                break
            }
        }
        
        for index in 0...36 { // y aixs count : 120 ~ 300 (body weight)
            let yVal = 120 + (index * 5)
            if Int(currentUser!.property.bodyWeight) == yVal {
                row = "\(index + 2)"
            }
        }
        let cell : BRACell = maleWorkSheet.cell(forCellReference: col+row)
        let maleBodyfatPercent = cell.floatValue()
        return maleBodyfatPercent
    }
    
    static func maleBodyFat() -> Float {
        
        return maleBodyFatPercent() * currentUser!.property.bodyWeight / 100
    }
    
    static func maleBodyMass() -> Float {
        return currentUser!.property.bodyWeight - maleBodyFat()
    }
    
    
    //MARK: - Female calculation
    static func femaleBodyFatPercent() -> Float {
        
        let path: String = Bundle.main.path(forResource: "table", ofType: "xlsx")!
        let spreadsheet: BRAOfficeDocumentPackage = BRAOfficeDocumentPackage.open(path)
        let femaleWorkSheet: BRAWorksheet = spreadsheet.workbook.worksheets[1] as! BRAWorksheet

        var tA: Float = 0
        var tB: Float = 0
        var tC: Float = 0
        
        /// calculate A, B, C from Hip, Abdomen, height
        for index in 0...62 { // 62 is total row of female table
            
            /// A
            let yAVal = 30 + (0.5 * Float(index))
            if currentUser!.property.averageHip == yAVal {
                let Acell: BRACell = femaleWorkSheet.cell(forCellReference: "B" + "\(index+2)")
                tA = Acell.floatValue()
            }
            
            /// B
            let yBVal = 20 + (0.5 * Float(index))
            if currentUser!.property.averageAbdomen == yBVal {
                let Bcell: BRACell = femaleWorkSheet.cell(forCellReference: "D" + "\(index+2)")
                tB = Bcell.floatValue()
            }
            
            /// C
            let yCVal = 55 + (0.5 * Float(index))
            if currentUser!.property.height == yCVal {
                let Ccell : BRACell = femaleWorkSheet.cell(forCellReference: "F" + "\(index+2)")
                tC = Ccell.floatValue()
            }
            
            if tA > 0 && tB > 0 && tC > 0 {
                break
            }
        }
        return tA + tB - tC
    }
    
    static func femaleBodyFat() -> Float {
        return femaleBodyFatPercent() * currentUser!.property.bodyWeight / 100.0
    }
    
    static func femaleBodyMass() -> Float {
        return currentUser!.property.bodyWeight - femaleBodyFat()
    }
    
    //MARK:- Both Male and Female
    static func dailyProtein(_ bodyMass: Float) -> Float {
        
        var dailyProtein: Float = 0
        for index in 0...arrActivityFactors.count - 1 {
            if arrActivityFactors[index] == currentUser!.property.activityFactor {
                dailyProtein = bodyMass * (0.5 + (0.1 * Float(index)))
                break
            }
        }
        return dailyProtein
    }
    
    static func totalBlocks(_ dailyProtein: Float) -> Float {
        return dailyProtein / 7.0
    }
    
    static func protein(_ totalBlocks: Float) -> Float {
        return totalBlocks * 7
    }
    
    static func carb(_ totalBlocks: Float) -> Float {
        return totalBlocks * 9
    }
    
    static func dailyFat(_ totalBlocks: Float) -> Float {
        return totalBlocks * 1.5
    }
}


















