//
//  Const.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//


import Foundation

var arrGenders = ["Male", "Female"]
var arrActivityFactors = ["Sedentary",
                          "Light: walking",
                          "Moderate: 30 mins per day, 3 x per week",
                          "Active: 1 hour per day, 5 x per week",
                          "Very Active: 2 hours per day, 5 x per week",
                          "Heavy weight training or 2x per day exercise4"]
var arrPointParmams = ["+1: No processed foods",
                       "+1: Not going 5 hours without earing and not eating more than 1/3 of daily blocks in single meal",
                       "+1: Consume at least 65% of body weight in ounces of water(8oz=1 glass)",
                       "+1: 1hour of physical activity or 1 WOD",
                       "+1 : 7+hours of sleep",
                       "+5: Include benchmart WOD for most improved, performed at beginning and end",
                       "Various amount: Allow challange bets",
                       "+1: Not going 5 hours without earing and not eating more than 1/3 of daily blocks in single meal",
                       "+1: Consume at least 65% of body weight in ounces of water(8oz=1 glass)",
                       "+1: 1hour of physical activity or 1 WOD",
                       "+1 : 7+hours of sleep",
                       "+5: Include benchmart WOD for most improved, performed at beginning and end",
                       "+5: Base points received for holding to Paleo restrictions for the day",
                       "+5: Base points received for holding to Keto restrictions for the day",
                       "+5: Base points received for Un-processed and clean eating restrictions"]

var arrGroupTypes = ["General health & wellness\nThis is the “lite” challange. If you feel like counting marcos and measuring / weighing food may be too difficult then this is for you. Base points are gathered on un processed foods and eating clean. (see library for more info)",
                     "Macro Pro\nThis is the macro counting staple of the \"Zone Diet\" created by Dr. Barry Sears. If you are serious about this challange, then this is the option for you. Your body measurements, determine your Daily macro Blocks. Base points are gathered on a macro requirement specific to each participant. (see library for more)",
                     "Paleo\nThis is the popular low carb, natural way of eating that is theorized how humans ate in the paleolithic era. This way of eating creates a low inflammatory response in the body and has been made wildly popular by Crossfitters around the globe. Receive 5 daily base points for holding to paleo restrictions",
                     "Keto\nIn this way of eating your body will primarily burn fats for fuel, not carbs(not for beginners). With a low carb intake, eating lots of fats is vital. To stay in a beneficial keto state, your daily macros need to be 65-75% fats, 25-30% protein, and 5-10% carbs. Stay in in the Keto ratio and receive your 5 daily base points"]
var arrGroupTypeTitles = ["Health&Wellness", "Macro Pro", "Paleo", "Keto"]

var arrBodyWeight = [Int]()
var arrHeight = [Float]()
var arrHips = [Float]()
var arrAbdomes = [Float]()
var arrWrist = [Float]()
var arrWaist = [Float]()
var isCompletedProfile = false
var isGranted = false
var rankingUsers = [RankingModel]()

var x_aixs = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL","AM","AN","AO","AP","AQ","AR","AS","AT","AU","AV","AW","AX","AY","AZ","BA","BB","BC","BD","BE","BF","BG"]

enum nPickerMode: Int {
    case gender = 0
    case weight
    case height
    case activityFactor
    case measurement1
    case measurement2
}

class Const {

    static let kClientID                    = "1093853424704-thncm1v0k50njukaecubd0llbilpkimv.apps.googleusercontent.com"
    
    static let SAVE_ROOT_PATH               = "Transform"
    
    // Social URLs
    static let facebookURL                  = "https://www.facebook.com/TransformMacros/"
    static let instagramURL                 = "https://www.instagram.com/transform_macros/"
    
    // button name
    static let APP_NAME                     = "Transform"
    static let OK                           = "OK"
    static let CANCEL                       = "Cancel"
    static let NO                           = "No"
    static let YES                          = "Yes"
    static let COME_SOON                    = "Transform will come soon in store"
    
    //PlaceHolder string
    static let PH_EMAILADDRESS              = "Email Address"
    static let PH_USERNAME                  = "User name"
    static let PH_PASSWORD                  = "Password"
    static let PH_CONFIRM_PASSWORD          = "Confirm password"
    
    //error messages
    
    static let CHECK_USERNAME_EMPTY         = "Please input your username"
    static let CHECK_PASSWORD_EMPTY         = "Please input your password"
    static let CHECK_EMAIL_EMPTY            = "Please input your email address"
    static let CHECK_EMAIL_INVALID          = "Please input valid email"
    static let CHECK_PASSWORD_MATCH         = "Password does not match"
    static let CHECK_AGREE_TERMS                  = "Please read and agree to terms of service"
    static let CHECK_BODY_WEIGHT_EMPTY      = "Please input your body weight"
    static let CHECK_HEIGHT_EMPTY           = "Please input your height"
    static let CHECK_VAILD_AGE              = "Please input your correct birthday"
    static let CHECK_WRIST_MEASUREMENT      = "Please input Wrist measurement"
    static let CHECK_WAIST_MEASUREMENT      = "Please input Waist measurement"
    static let CHECK_HIP_MEASUREMENT        = "Please input Hip Measurement"
    static let CHECK_ABDMEN_MEASUREMENT     = "Please input Abdomen Measurement"
    static let CHECK_INVVALIDE_WARIST_WAIST = "Invaild wrist and waist measurement is inputed.\n Difference must be more than 22.0"
    
    static let INPUT_CHALLANGENAME          = "Please input challange name"
    static let CHECK_VAILD_DATE             = "Please input vaild date"
    static let SELECT_MODE                  = "Please select public or private mode"
    static let SELECT_TYPE                  = "Please select group type"
    static let SELECT_P_SYSTEM              = "Please select point system"
    static let INPUT_COST                   = "Please input cost"
    static let INPUT_NAME                   = "Please input name to invite"
    static let INPUT_EMAIL                  = "Please input email address to invite"
    static let INPUT_VALIDEMAIL             = "Please input vaild email address"
    static let INPUT_PHONE                  = "Please input phone number to invite"
    static let INVALID_SUBMIT               = "You have to invite at least 1 member"
    static let MEMBER_ADDED                 = "Member is added successfully"
    static let INVITATION_SENT              = "Invitation sent"
    static let NON_EXIST_MEMBER             = "Member does not exist system\nDo you want to invite outside?"
    static let ERROR_CONNECT                = "Failed to server connection"
    static let EXIST_EMAIL                  = "User is already registered"
    static let UNREGISTERED_USER            = "User does not registered"
    static let WRONG_PASSWORD               = "Wrong password"
    static let EXIST_GROUP                  = "Group already exist"
    static let COMPLETE_PROFILE             = "Edit profile page to calculate optimal Macro intake"
    static let ALERT_TITLE                  = "Enter the macro blocks you are consuming each day"
    static let SUBMIT                       = "Submit"
    static let ALERT_WOD                    = "If you choose this option then you will be able to grant anyone 5 points. You will need to create any workout and let your members know what it is. After the workout has been performed at begining and end, you will award the 5 points to the most improved member from your dashboard page.\n\nInclude Bench mark WOD in your challenge?"
    static let ALERT_FULLBLOCK              = "You ate full blocks"
    static let ALERT_GRANT                  = "Grant this member the 5 benchmark WOD points"
    static let MSG_PAYMENT_FAILED           = "Payment process failed"
    static let MSG_ADD_YOURSELF             = "If you want to participate in the group, please add yourself also, then accept your own invitation"
    static let MSG_CONGRATULATION_GROUP     = "Congrats on making your group! Make sure everyone is ready to participate by the "
    static let MSG_ALREADY_BEANCHMARK       = "You have already granted the 5 benchmark WOD points"
    static let MSG_INVALID_BEANCHMARK       = "You can't give benchmark to yourself"
    static let MSG_FINISHEDGROUP            = " has finished! Congratulations to the winners, prizes will be awarded. Keep up your diet and join a new group with us today!"
    static let BLOCK_MSG                    = "This content has been blocked"
    static let BLOCK_CONFIRM                = "Are you sure want to block this comment?"
    static let BLOCK_DESCRIPTION            = "You can block comment by click message or the user's profile photo"
    
    // Params
    
    static let PARAM_USERMODEL              = "userModel"
    static let PARAM_ID                     = "id"
    static let PARAM_USERNAME               = "userName"
    static let PARAM_EMAIL                  = "email"
    static let PARAM_PASSWORD               = "password"
    static let PARAM_USERID                 = "userId"
    static let PARAM_STATUS                 = "status"
    static let PARAM_PHOTOURL               = "photoUrl"
    static let PARAM_TOKEN                  = "token"
    
    static let PARAM_PROPERTYMODEL          = "propertyModel"
    static let PARAM_GENDER                 = "gender"
    static let PARAM_BIRTHDAY               = "birthday"
    static let PARAM_AGE                    = "age"
    static let PARAM_BODYWEIGHT             = "bodyWeight"
    static let PARAM_HEIGHT                 = "height"
    static let PARAM_ACTIVITYFACTOR         = "activityFactor"
    static let PARAM_WRIST_MEASUREMENT      = "wristMeasurement"
    static let PARAM_WAIST_MEASUREMENT      = "waistMeasurement"
    static let PARAM_AVERAGEHIP             = "averageHip"
    static let PARAM_AVERAGEABDOMEN         = "averageAbdomen"
    static let PARAM_STARTINGBODYWEIGHT     = "startingBodyWeight"
    
    static let PARAM_MACROMODEL             = "macroModel"
    static let PARAM_BODYFAT                = "bodyFat"
    static let PARAM_BODYMASS               = "bodyMass"
    static let PARAM_DAILYPROTEIN           = "dailyProtein"
    static let PARAM_DAILYCARB              = "dailyCarb"
    static let PARAM_DAILYFAT               = "dailyFat"
    static let PARAM_TOTALBLOCKS            = "totalBlocks"
    static let PARAM_EATBLOCKTILLNOW        = "eatBlocksTillNow"
    static let PARRAM_STARTBODYFATPER       = "startingBodyFatPercent"
    
    static let PARAM_POINTMODELLIST         = "pointModelList"
    static let PARAM_TODAYPOINTS            = "todayPoints"
    static let PARAM_TOTALPOINTS            = "totalPoints"
    static let PARAM_EATBLOCKSTILLLNOW      = "eatBlocksTillNow"
    static let PARAM_BENCHMARK              = "benchmark"
    
    static let PARAM_GROUPID                = "groupId"
    static let PARAM_CREATORID              = "creatorId"
    static let PARAM_GROUPNAME              = "groupName"
    static let PARAM_GROUPKIND              = "kindOfGroup"
    static let PARAM_GROUPTYPE              = "groupType"
    static let PARAM_POINTPARAMS            = "pointParameter"
    static let PARAM_PRICE                  = "price"
    static let PARAM_CREATORNAME            = "creatorName"
    static let PARAM_STARTDATE              = "startDate"
    static let PARAM_ENDDATE                = "endDate"
    static let PARAM_FINISHED               = "challengeFinished"
    static let PARAM_READCOUNT              = "readCount"
    static let PARAM_MSGCONTENT             = "messageContent"
    static let PARAM_MESSAGEID               = "messageId"
    static let PARAM_MSGTYPE                = "msgType"
    static let PARAM_CONTENT                = "content"
    static let PARAM_POINTCNT               = "pointCnt"
    
    static let PARAM_CARDNUMBER             = "cardNumber"
    static let PARAM_EXPIRATIONDATE         = "expirationDate"
    static let PARAM_CARDCODE               = "cardCode"
    static let PARAM_AMOUNT                 = "amount"
    
    //Response params
    static let PARAM_PUBLICGROUP            = "publicGroup"
    static let PARAM_INVITEDGROUP           = "invitedGroup"
    static let PARAM_USERLIST               = "userList"
    static let PARAM_GROUPLIST              = "groupList"
    static let PARAM_CHATLIST               = "chatList"
    static let PARAM_RANKINGLIST            = "rankingList"
    
    static let PARAM_USERMODELLIST          = "userModelList"
    static let PARAM_LIBRARY                = "library"
    static let PARAM_HELP                   = "help"
    static let PARAM_TITLE                  = "title"
    static let PARAM_DESCRIPTION            = "description"
    static let PARAM_PREVIEWPIC             = "preview_picture"
    static let PARAM_FILEURL                = "file_url"
    
    // Key
    static let KEY_ID                       = "id"
    static let KEY_TOKEN                    = "token"
    static let KEY_EMAIL                    = "email"
    static let KEY_PASSWORD                 = "password"
    static let KEY_PRIVATE                  = "private"
    static let KEY_PUBLIC                   = "public"
    static let KEY_GENERAL                  = "general"
    static let KEY_MACRO                    = "macro"
    static let KEY_PARAM1                   = "param1"
    static let KEY_PARAM2                   = "param2"
    static let KEY_PARAM3                   = "param3"
    static let KEY_PARAM4                   = "param4"
    static let KEY_PARAM5                   = "param5"
    static let KEY_PARAM6                   = "param6"
    static let KEY_PARAM7                   = "param7"
    static let KEY_PARAM8                   = "param8"
    static let KEY_PARAM9                   = "param9"
    static let KEY_PARAM10                  = "param10"
    static let KEY_PARAM11                  = "param11"
    static let KEY_PARAM12                  = "param12"
    static let KEY_PARAM_PALEO_BASE         = "param13"
    static let KEY_PARAM_KETO_BASE          = "param14"
    static let KEY_PARAM_GENERAL_BASE       = "param15"
    
    static let KEY_PUSH_CHAT                = "chat"
    static let KEY_PUSH_INVITE              = "invite"
    static let KEY_PUSH_BENCHMARK           = "benchmark"
    
    static let KEY_CHECKED_PARAMS           = "key_checkedParams"
    static let KEY_SAVEDDATE                = "key_saved_date"
    
    static let FROM_GALLERY                 = "Gallery"
    static let FROM_CAMERA                  = "Camera"
    
    static let STR_REPORT                   = "Report content"
    static let STR_BLOCK                    = "Block content"
    
    // Result code
    static let RESULT_CODE                  = "resultCode"
    static let RESULT_SUCCESS               = "result_success"
    static let CODE_FAIL                    = -100
    static let CODE_0                       = 0
    static let CODE_SUCCESS                 = 100
    static let CODE_UNREGISTERED_USER       = 101
    static let CODE_INVALIDE_EMAIL_PWD      = 102
    static let CODE_ALREADY_REGISTERED      = 103
    static let CODE_FIRST_SOCIAL_LOGIN      = 104
    static let CODE_GROUP_EXIST             = 105    
    
    static let lightBlackColor              = UIColor.init(red: 30/255.0, green: 30/255.0, blue: 30/255.0, alpha: 1)
    
    
    
    
}
