//
//  ParseHelper.swift
//  Transform
//
//  Created by Yin on 26/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import Foundation
import SwiftyJSON

class ParseHelper {
    
    static func parseUserWithProperty(_ json: JSON) -> UserModel {
        let user = UserModel()
        let userObject = json[Const.PARAM_USERMODEL]
        user.userId = userObject[Const.PARAM_ID].intValue
        user.userName = userObject[Const.PARAM_USERNAME].stringValue
        user.email = userObject[Const.PARAM_EMAIL].stringValue
        user.password = userObject[Const.PARAM_PASSWORD].stringValue
        user.photoUrl = userObject[Const.PARAM_PHOTOURL].stringValue

        user.property = PropertyModel()
        let propertyObject = json[Const.PARAM_PROPERTYMODEL]
        user.property.gender = propertyObject[Const.PARAM_GENDER].stringValue
        user.property.birthday = propertyObject[Const.PARAM_BIRTHDAY].stringValue
        user.property.age = propertyObject[Const.PARAM_AGE].intValue
        user.property.bodyWeight = propertyObject[Const.PARAM_BODYWEIGHT].floatValue
        user.property.height = propertyObject[Const.PARAM_HEIGHT].floatValue
        user.property.activityFactor = propertyObject[Const.PARAM_ACTIVITYFACTOR].stringValue
        user.property.wristMeasurement = propertyObject[Const.PARAM_WRIST_MEASUREMENT].floatValue
        user.property.waistMeasurement = propertyObject[Const.PARAM_WAIST_MEASUREMENT].floatValue
        user.property.averageHip = propertyObject[Const.PARAM_AVERAGEHIP].floatValue
        user.property.averageAbdomen = propertyObject[Const.PARAM_AVERAGEABDOMEN].floatValue
        user.property.startingBodyWeight = propertyObject[Const.PARAM_STARTINGBODYWEIGHT].floatValue
        
        user.macro = MacroModel()
        let macroObject = json[Const.PARAM_MACROMODEL]
        user.macro.bodyFat = macroObject[Const.PARAM_BODYFAT].floatValue
        user.macro.bodyMass = macroObject[Const.PARAM_BODYMASS].floatValue
        user.macro.dailyProtein = macroObject[Const.PARAM_DAILYPROTEIN].floatValue
        user.macro.dailyCarb = macroObject[Const.PARAM_DAILYCARB].floatValue
        user.macro.dailyFat = macroObject[Const.PARAM_DAILYFAT].floatValue
        user.macro.totalBlocks = Int(macroObject[Const.PARAM_TOTALBLOCKS].floatValue)
        user.macro.eatBlocksTillNow = Int(macroObject[Const.PARAM_EATBLOCKTILLNOW].floatValue)
        user.macro.startingBodyFatPercent = macroObject[Const.PARRAM_STARTBODYFATPER].floatValue
        
        user.points = [PointModel]()
        let pointList = json[Const.PARAM_POINTMODELLIST].arrayValue
        for pointDic in pointList {
            let point = PointModel()
            point.groupId = pointDic[Const.PARAM_GROUPID].intValue
            point.todayPoints = pointDic[Const.PARAM_TODAYPOINTS].intValue
            point.totalPoints = pointDic[Const.PARAM_TOTALPOINTS].intValue
            user.points.append(point)
        }
        
        return user        
    }
    
    static func parseUser(_ json: JSON) -> UserModel {
        
        let user = UserModel()
        let userObject = json[Const.PARAM_USERMODEL]
        user.userId = userObject[Const.PARAM_ID].intValue
        user.userName = userObject[Const.PARAM_USERNAME].stringValue
        user.email = userObject[Const.PARAM_EMAIL].stringValue
        user.password = userObject[Const.PARAM_PASSWORD].stringValue
        user.photoUrl = userObject[Const.PARAM_PHOTOURL].stringValue
        
        return user        
    }
    
    static func parseProperty(_ json: JSON) -> PropertyModel {
        
        let property = PropertyModel()
        let propertyObject = json[Const.PARAM_PROPERTYMODEL]
        property.gender = propertyObject[Const.PARAM_GENDER].stringValue
        property.birthday = propertyObject[Const.PARAM_BIRTHDAY].stringValue
        property.age = propertyObject[Const.PARAM_AGE].intValue
        property.bodyWeight = propertyObject[Const.PARAM_BODYWEIGHT].floatValue
        property.height = propertyObject[Const.PARAM_HEIGHT].floatValue
        property.activityFactor = propertyObject[Const.PARAM_ACTIVITYFACTOR].stringValue
        property.wristMeasurement = propertyObject[Const.PARAM_WRIST_MEASUREMENT].floatValue
        property.waistMeasurement = propertyObject[Const.PARAM_WAIST_MEASUREMENT].floatValue
        property.averageHip = propertyObject[Const.PARAM_AVERAGEHIP].floatValue
        property.averageAbdomen = propertyObject[Const.PARAM_AVERAGEABDOMEN].floatValue
        
        return property
    }
    
    static func parseGROUP(_ json: JSON) -> GroupModel {
        
        let group = GroupModel()
        group.id = json[Const.PARAM_ID].intValue
        group.creatorId = json[Const.PARAM_CREATORID].intValue
        group.creatorName = json[Const.PARAM_CREATORNAME].stringValue
        group.groupName = json[Const.PARAM_GROUPNAME].stringValue
        group.kindOfGroup = json[Const.PARAM_GROUPKIND].stringValue
        group.groupType = json[Const.PARAM_GROUPTYPE].stringValue
        group.pointParams = json[Const.PARAM_POINTPARAMS].stringValue
        group.price = json[Const.PARAM_PRICE].floatValue
        group.startDate = json[Const.PARAM_STARTDATE].stringValue
        group.endDate = json[Const.PARAM_ENDDATE].stringValue
        group.challengeFinished = json[Const.PARAM_FINISHED].intValue
        
        return group
    }
    
    static func parseGroupUser(_ json: JSON) -> UserModel {
        
        let user = UserModel()
        user.userId = json[Const.PARAM_ID].intValue
        user.userName = json[Const.PARAM_USERNAME].stringValue
        user.groupStatusIn = json[Const.PARAM_STATUS].stringValue
        
        return user
    }
    
    static func parseMessage(_ json: JSON) -> MessageModel {
        
        let message = MessageModel()
        message.messageId = json[Const.PARAM_MESSAGEID].intValue
        message.userId = json[Const.PARAM_USERID].intValue
        message.userName = json[Const.PARAM_USERNAME].stringValue
        message.photoUrl = json[Const.PARAM_PHOTOURL].stringValue
        message.messageContent = json[Const.PARAM_MSGCONTENT].stringValue
        
        return message
    }
    
    static func parseRanking(_ json: JSON) -> RankingModel {
        
        let ranking = RankingModel()
        ranking.userId = json[Const.PARAM_USERID].intValue
        ranking.userName = json[Const.PARAM_USERNAME].stringValue
        ranking.photoUrl = json[Const.PARAM_PHOTOURL].stringValue
        ranking.totalPoints = json[Const.PARAM_TOTALPOINTS].intValue
        ranking.benchmark = json[Const.PARAM_BENCHMARK].intValue
        
        return ranking
    }
    
    static func parseLibrary(_ json: JSON) -> LibraryModel {
        
        let library = LibraryModel()
        
        library.id = json[Const.PARAM_ID].intValue
        library.title = json[Const.PARAM_TITLE].stringValue
        library.description = json[Const.PARAM_DESCRIPTION].stringValue
        library.preview_picture = json[Const.PARAM_PREVIEWPIC].stringValue
        library.file_url = json[Const.PARAM_FILEURL].stringValue
        
        return library
    }
    
}
