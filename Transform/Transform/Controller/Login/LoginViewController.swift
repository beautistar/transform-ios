//
//  LoginViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftyJSON
import GoogleSignIn
import Alamofire

class LoginViewController: BaseViewController, UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate {    

    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var gSignInButton: GIDSignInButton!
    @IBOutlet weak var imvChkBox: UIImageView!
    
    var isChecked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        GIDSignIn.sharedInstance().clientID = Const.kClientID
        
        initData()
        
        initView()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func initData() {
        
        let arrBodyWeightCnt = 37
        let arrHeightCnt = 43
        let arrHipesCnt = 61
        let arrWristCnt = 16
        let arrWaistCnt = 81
        
        arrBodyWeight = [Int]()
        arrHeight = [Float]()
        arrWrist = [Float]()
        arrWaist = [Float]()
        arrHips = [Float]()
        arrAbdomes = [Float]()
        
        /// set body weight array
        for index in 0...arrBodyWeightCnt - 1{
            arrBodyWeight.append(120 + (index * 5))
        }
        
        /// set height array
        for index in 0...arrHeightCnt - 1 {
            arrHeight.append(55.0 + (Float(index) * 0.5))
        }
        
        /// set Wrist measurement array
        for index in 0...arrWristCnt - 1 {
            arrWrist.append(3.5 + (Float(index) * 0.5))
        }
        
        /// set Waist measurement array
        for index in 0...arrWaistCnt - 1 {
            arrWaist.append(20 + (Float(index) * 0.5))
        }
        
        /// set Hip array
        for index in 0...arrHipesCnt - 1 {
            arrHips.append(30 + (Float(index) * 0.5))
        }
        
        /// set Abdomen array
        for index in 0...arrHipesCnt - 1 {
            arrAbdomes.append(20 + Float(index) * 0.5)
        }
    }
    
    func initView() {
        
        imvChkBox.layer.borderColor = UIColor.white.cgColor
        imvChkBox.image = #imageLiteral(resourceName: "bg_checkbox_unchecked")
        isChecked = false
        
        tfEmail.attributedPlaceholder = NSAttributedString.init(string: Const.PH_EMAILADDRESS, attributes: [NSAttributedStringKey.foregroundColor:UIColor.darkGray])
        tfPassword.attributedPlaceholder = NSAttributedString.init(string: Const.PH_PASSWORD, attributes: [NSAttributedStringKey.foregroundColor:UIColor.darkGray])
        tfPassword.isSecureTextEntry = true
        
        let prevUser = UserDefaults.standard.string(forKey: Const.KEY_EMAIL)
        
        if prevUser != "" {
            tfEmail.text = UserDefaults.standard.string(forKey: Const.KEY_EMAIL)
            tfPassword.text = UserDefaults.standard.string(forKey: Const.KEY_PASSWORD)
        }
        if isCompletedProfile {
            self.doLogin()
        }
    }
    
    @IBAction func termTapped(_ sender: Any) {
        
        imvChkBox.image = #imageLiteral(resourceName: "bg_checkbox_checked")
        isChecked = true
        
        let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        self.navigationController?.pushViewController(termsVC, animated: true)
        
        
    }
    
    
    func isValid() -> Bool {
        
        if tfEmail.text?.count == 0 {
            
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if !isChecked {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_AGREE_TERMS, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        if isValid() {
            self.doLogin()
        }
    }
    
    func gotoHome() {
        
        // save token
        ApiRequest.saveToken(token: (currentUser?.token)!) { (rescode) in
            
        }
        
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    func gotoPersonalinfo() {
        let personalInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoViewController") as! PersonalInfoViewController
        self.navigationController?.pushViewController(personalInfoVC, animated: true)
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        
        tfEmail.resignFirstResponder()
        tfPassword.resignFirstResponder()
        
        let resetVC = self.storyboard?.instantiateViewController(withIdentifier: "ResetViewController") as! ResetViewController
        self.navigationController?.pushViewController(resetVC, animated: true)
    }
    
    @IBAction func fbLoginAction(_ sender: Any) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil) {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), gender, email"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    
                    let jsonResponse = JSON(result!)
                    
                    print("jsonResponse", jsonResponse)
                    
                    let id = jsonResponse["id"].stringValue
                    let email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
                    let name = jsonResponse["name"].string ?? "unknown"
                    
                    self.loginSocial(username: name, email: email, password: id)
                    
                } else {
                    // TODO :  Exeption  ****
                    print(error!)
                }
            })
        } else {
            print("token is null")
        }
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        tfEmail.resignFirstResponder()
        tfPassword.resignFirstResponder()
        
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
    @IBAction func googleLoginAction(_ sender: Any) {
        
        //if GIDSignIn.sharedInstance().currentUser == nil {

            GIDSignIn.sharedInstance().signIn()
            
        //}
    }
    
    func doLogin() {
        
        let email = tfEmail.text!
        let password = tfPassword.text!
        
        self.showLoadingView()
        
        ApiRequest.login(email: email, password: password, completion: { (message)  in
            
            self.hideLoadingView()
            if message == Const.RESULT_SUCCESS {
                ApiRequest.saveToken(token: currentUser!.token, completion: { (resultCode) in                    
                })
                self.gotoHome()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: message, positive: Const.OK, negative: nil)
            }
        })
    }
    
    func loginSocial(username: String, email: String, password: String) {
        
        self.showLoadingView()
        
        ApiRequest.loginSocial(username: username, email: email, password: password, completion: { (resultCode)  in
            
            self.hideLoadingView()
            if resultCode == Const.CODE_SUCCESS {
                self.gotoHome()
            } else if resultCode == Const.CODE_FIRST_SOCIAL_LOGIN {
                self.gotoPersonalinfo()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        })
    }
    
    // MARK:- GIDSignInUIDelegate
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        //myActivityIndicator.stopAnimating()
        self.hideLoadingView()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID ?? "0"                  // For client-side use only!
            //let idToken = user.authentication.idToken ?? "" // Safe to send to the server
            let name = user.profile.name ?? "unknown"
            let givenName = user.profile.givenName ?? "unknown"
            let familyName = user.profile.familyName ?? "unknown"
            let email = user.profile.email ?? String(format: "%@@google.com", userId)
            
            loginSocial(username: name, email: email, password: userId)
            
            print("userId", userId)
            print("fullname", name)
            print("givenName", givenName)
            print("familyName", familyName)
            print("email", email)
            
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
                withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
        print("disconnected")
    }
    
    // MARK: - TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfEmail.resignFirstResponder()
        tfPassword.resignFirstResponder()
        return true
    }
}
