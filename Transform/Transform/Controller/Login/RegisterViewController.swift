//
//  RegisterViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    @IBOutlet weak var imvChkBox: UIImageView!
    var user = UserModel()
    
    var isChecked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        imvChkBox.layer.borderColor = UIColor.white.cgColor
        imvChkBox.image = #imageLiteral(resourceName: "bg_checkbox_unchecked")
        isChecked = false
        
        tfEmail.attributedPlaceholder = NSAttributedString.init(string: Const.PH_EMAILADDRESS, attributes: [NSAttributedStringKey.foregroundColor:UIColor.darkGray])
        tfEmail.keyboardType = UIKeyboardType.emailAddress
        tfUsername.attributedPlaceholder = NSAttributedString.init(string: Const.PH_USERNAME, attributes: [NSAttributedStringKey.foregroundColor:UIColor.darkGray])
        tfPassword.attributedPlaceholder = NSAttributedString.init(string: Const.PH_PASSWORD, attributes: [NSAttributedStringKey.foregroundColor:UIColor.darkGray])
        tfPassword.isSecureTextEntry = true
        tfConfirmPassword.attributedPlaceholder = NSAttributedString.init(string: Const.PH_CONFIRM_PASSWORD, attributes: [NSAttributedStringKey.foregroundColor:UIColor.darkGray])
        tfConfirmPassword.isSecureTextEntry = true
    }
    
    @IBAction func termTapped(_ sender: Any) {
        
        imvChkBox.image = #imageLiteral(resourceName: "bg_checkbox_checked")
        isChecked = true
        
        let termsVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        self.navigationController?.pushViewController(termsVC, animated: true)
    
    }
    
    func isValid() -> Bool {
        
        if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_EMAIL_INVALID, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfUsername.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_USERNAME_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfPassword.text != tfConfirmPassword.text {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_PASSWORD_MATCH, positive: Const.OK, negative: nil)
            return false
        }
        
        if !isChecked {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_AGREE_TERMS, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func registerAction(_ sender: Any) {
        
        if isValid() {
            
            /// do register
            
            user.email = tfEmail.text!
            user.userName = tfUsername.text!
            user.password = tfPassword.text!
            
            self.showLoadingView()
            ApiRequest.register(user, completion: { (resultCode, user_id)  in
                self.hideLoadingView()
                
                if resultCode == Const.CODE_SUCCESS {
                    self.user.userId = user_id
                    currentUser = self.user
                    self.gotoPersonalInfoVC()
                } else if resultCode == Const.CODE_ALREADY_REGISTERED {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.EXIST_EMAIL, positive: Const.OK, negative: nil)
                } else {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
                }
            })
        }
    }
    
    func gotoPersonalInfoVC() {
        let personalVC = self.storyboard?.instantiateViewController(withIdentifier: "PersonalInfoViewController") as! PersonalInfoViewController
        self.navigationController?.pushViewController(personalVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        tfEmail.resignFirstResponder()
        tfPassword.resignFirstResponder()
        tfUsername.resignFirstResponder()
        tfConfirmPassword.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfEmail.resignFirstResponder()
        tfPassword.resignFirstResponder()
        tfUsername.resignFirstResponder()
        tfConfirmPassword.resignFirstResponder()
    
        return true
    }
}
