//
//  ResetViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class ResetViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var tfEmailAddr:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        tfEmailAddr.attributedPlaceholder = NSAttributedString.init(string: Const.PH_EMAILADDRESS, attributes: [NSAttributedStringKey.foregroundColor:UIColor.darkGray])
    }
    
    @IBAction func onBack(_ sender : Any) {
     
        tfEmailAddr.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onResetPassword(_ sender : Any) {
    
        tfEmailAddr.resignFirstResponder()
        /// reset password API
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - UITextFieldDelegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfEmailAddr.resignFirstResponder()
        return true
    }
    
}
