//
//  PersonalInfoViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class PersonalInfoViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var vwData: UIView!
    @IBOutlet weak var vwDataPicker: UIPickerView!
    @IBOutlet weak var vwBirthday: UIView!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var vwBirthdayDatePicker: UIDatePicker!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var tfBodyWeight: UITextField!
    @IBOutlet weak var tfHeight:UITextField!
    @IBOutlet weak var lblActivityFactor: UILabel!
    
    var nMode: nPickerMode = nPickerMode.gender
    var nGender:Int = 0
    var nActivityFactor:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initData() {
        
        vwBirthdayDatePicker.date = Date()
       
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        lblBirthday.text = formatter.string(from: Date())
        let calendar = Calendar.current
        let component1 = calendar.dateComponents([.day, .month, .year, .hour], from: vwBirthdayDatePicker.date)
        let component2 = calendar.dateComponents([.day, .month, .year], from: Date())
        lblAge.text = String(component2.year! - component1.year!)
        
        print(currentUser?.userId ?? 0)
    }
    
    func isValid() -> Bool {
        
        if Int(lblAge.text!)! < 1 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_VAILD_AGE, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfBodyWeight.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_BODY_WEIGHT_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfHeight.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_HEIGHT_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func onSave(_ sender: Any) {
        
        if isValid() {
            
            // save token
            
            ApiRequest.saveToken(token: (currentUser?.token)!, completion: { (resCode) in
                
            })
            
            /// upload profile informatoin
            
            let property = PropertyModel()
            
            property.gender = lblGender.text!
            property.birthday = lblBirthday.text!
            property.age = Int(lblAge.text!)!
            property.height = Float(tfHeight.text!)!
            property.startingBodyWeight = Float(tfBodyWeight.text!)!
            property.bodyWeight = Float(tfBodyWeight.text!)!
            property.activityFactor = lblActivityFactor.text!
            
            self.showLoadingView()
            ApiRequest.uploadProperty(userId: (currentUser?.userId)!, property: property, completion: { (resultCode)  in
                self.hideLoadingView()
                if resultCode == Const.CODE_SUCCESS {
                    currentUser?.property = property
                    self.gotoHome()
                } else {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
                }
            })
        }
    }
    
    func gotoHome() {
     
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    @IBAction func onBack(_ sender : Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onGender(_ sender : Any) {
        nMode = .gender
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(nGender, inComponent: 0, animated: false)
    }
    
    @IBAction func onSelectGender(_ sender:Any) {
        vwData.isHidden = true
        let nRow = vwDataPicker.selectedRow(inComponent: 0)
        switch nMode {
        case .gender:
            nGender = vwDataPicker.selectedRow(inComponent: 0)
            lblGender.text = arrGenders[nGender]
        case .activityFactor:
            nActivityFactor = vwDataPicker.selectedRow(inComponent: 0)
            lblActivityFactor.text = arrActivityFactors[nActivityFactor]
        case .weight:
            tfBodyWeight.text = String(arrBodyWeight[nRow])
        case .height:
            tfHeight.text = String(arrHeight[nRow])
        default: break
        }
    }
    @IBAction func onBodyWeight(_ sender: Any) {
        nMode = .weight
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(0, inComponent: 0, animated: false)
    }
    
    @IBAction func onHeight(_ sender: Any) {
        nMode = .height
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(0, inComponent: 0, animated: false)
    }
    
    @IBAction func onActivtyFactor(_ sender : Any) {
        nMode = .activityFactor
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(nActivityFactor, inComponent: 0, animated: false)
    }
    
    @IBAction func onBirthday(_ sender : Any) {
        vwBirthday.isHidden = false
    }
    
    @IBAction func onSelectBirthDate(_ sender : Any) {
        vwBirthday.isHidden = true
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        lblBirthday.text = dateFormatter.string(from: vwBirthdayDatePicker.date)
        let component1 = Calendar.current.dateComponents([.day, .month, .year], from: vwBirthdayDatePicker.date)
        let component2 = Calendar.current.dateComponents([.day, .month, .year], from: Date())
        lblAge.text = String(component2.year! - component1.year!)
    }
    
    //MARK: - UIPickerViewDataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch nMode {
        case .gender:
            return arrGenders.count
        case .weight:
            return arrBodyWeight.count
        case .height:
            return arrHeight.count
        case .activityFactor:
            return arrActivityFactors.count
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch nMode {
        case .gender:
            return arrGenders[row]
        case .activityFactor:
            return arrActivityFactors[row]
        case .weight:
            return String(arrBodyWeight[row])
        case .height:
            return String(arrHeight[row])
        default: return ""
        }
    }
    
    //MARK: - UITextFieldDelegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfBodyWeight.resignFirstResponder()
        tfHeight.resignFirstResponder()
        
        return true
    }
}
