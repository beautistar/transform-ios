//
//  JoinGroupViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class JoinGroupViewController: BaseViewController, SlideNavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var inviteTableView: UITableView!
    
    var width: CGFloat = 0
    var invitedGroupInfo = [GroupModel]()
    var publicGroupInfo = [GroupModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        inviteTableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        width = collectionView.frame.size.width
        
        getJoinGroup()
    }

    @IBAction func onMenu(_ sender:Any) {
        
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }

    
    func getJoinGroup() {
        
        invitedGroupInfo.removeAll()
        publicGroupInfo.removeAll()
        
        showLoadingView()
        
        ApiRequest.getJoinGroupInfo(userId: (currentUser?.userId)!) { (resultCode, invitedGroups, publicGroups) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {
                
                self.invitedGroupInfo = invitedGroups
                self.publicGroupInfo = publicGroups
                
                self.inviteTableView.reloadData()
                self.collectionView.reloadData()
                
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func gotoDetailGroup(_ group: GroupModel) {
        
        let groupDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "GroupDetailViewController") as! GroupDetailViewController
        groupDetailVC.selectedGroup = group
        self.navigationController?.pushViewController(groupDetailVC, animated: true)
    }
    
    //MARK:- TableView Delegate & datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invitedGroupInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "InviteCell")
        cell.textLabel?.font = UIFont.init(name: "Eras Light ITC", size: 15)
        cell.textLabel?.backgroundColor = UIColor.clear
        cell.textLabel?.text = invitedGroupInfo[indexPath.row].groupName + "(" + invitedGroupInfo[indexPath.row].creatorName + ")"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        gotoDetailGroup(invitedGroupInfo[indexPath.row])
    }
    
    //MARK:- CollectionViewDelegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return publicGroupInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PublicGroupCell", for: indexPath) as! PublicGroupCell
        
        cell.lblName.text = publicGroupInfo[indexPath.row].groupName + "(" + publicGroupInfo[indexPath.row].creatorName + ")"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: width, height: 30.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        gotoDetailGroup(publicGroupInfo[indexPath.row])
    }
}
