//
//  CreateGroupViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit
import MessageUI

class CreateGroupViewController: BaseViewController, SlideNavigationControllerDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout/*, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate*/ {
    

    @IBOutlet weak var lblStepTitle: UILabel!
    @IBOutlet weak var vwGroupname: UIView!
    @IBOutlet weak var tfChallengename: UITextField!
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    @IBOutlet weak var vwDatePker: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var lblPrivateDesc: UILabel!
    @IBOutlet weak var lblPublicDesc: UILabel!
    @IBOutlet weak var vwType: UIView!
    @IBOutlet weak var vwPointSystem: UIView!
    @IBOutlet weak var vwCheckbox1: UIImageView!
    @IBOutlet weak var vwCheckbox2: UIImageView!
    @IBOutlet weak var vwCheckbox3: UIImageView!
    @IBOutlet weak var vwCheckbox4: UIImageView!
    @IBOutlet weak var vwCheckbox5: UIImageView!
    @IBOutlet weak var vwCheckbox6: UIImageView!
    @IBOutlet weak var vwCheckbox7: UIImageView!
    @IBOutlet weak var vwGenGroupPointSys: UIView!
    @IBOutlet weak var vwCheckBox8: UIImageView!
    @IBOutlet weak var vwCheckBox9: UIImageView!
    @IBOutlet weak var vwCheckBox10: UIImageView!
    @IBOutlet weak var vwCheckBox11: UIImageView!
    @IBOutlet weak var vwCheckBox12: UIImageView!
    @IBOutlet weak var vwCost: UIView!
    @IBOutlet weak var tfCost: UITextField!
    @IBOutlet weak var vwInviteMembers: UIView!
    @IBOutlet weak var lblTypeTitle: UILabel!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    //@IBOutlet weak var vwInviteMembersScroll: UIScrollView!
    @IBOutlet weak var tblInviteMembers: UITableView!
    //@IBOutlet weak var cvGroupType: UICollectionView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblType1: UILabel!
    @IBOutlet weak var lblType4: UILabel!
    @IBOutlet weak var lblType3: UILabel!
    @IBOutlet weak var lblType2: UILabel!
    
    var nGroupMode: Int = 0
    var nGroupType: Int = 0
    var nSelectedPointSystem: Int = 0
    var selectedValue = 0
    var nSelectedDate: Int = 0
    var arrInviteMembers = [InviteModel]();
    var formatter: DateFormatter!
    var isValidDate: Bool = false
    var createdGroupId: Int = 0
    var params = [Bool]()
    var pointCheckBoxes = [UIImageView]()
    var pointParameter = ""
    var cvWidth: CGFloat = 0.0
    var cvHeight: CGFloat = 0.0
    var typeLabels = [UILabel]()
    var arrSearchUsers = [UserModel]()
    @IBOutlet weak var cvSearchUserList: UICollectionView!
    
    var inviteMemberId = -1
    
    
    //MARK:- live cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        lblStepTitle.text = "Group name"
        vwGroupname.isHidden = false
        vwType.isHidden = true
        vwPointSystem.isHidden = true
        vwCost.isHidden = true
        vwInviteMembers.isHidden = true
        nGroupMode = -1;
        nGroupType = -1;
        nSelectedPointSystem = -1;
        nSelectedDate = 1;
        isValidDate = true
        createdGroupId = 0
        
        arrInviteMembers = [InviteModel]();
        arrInviteMembers.removeAll()
        
        /// set initial start date
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        datePicker.date = Date()
        
        formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        tfStartDate.text = formatter.string(from: Date())
        tfEndDate.text = formatter.string(from: Date())
        
        tfName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tfEmail.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        for _ in 0...11 {
            params.append(false)
        }
        
        pointParameter = ""
        pointCheckBoxes.append(vwCheckbox1)
        pointCheckBoxes.append(vwCheckbox2)
        pointCheckBoxes.append(vwCheckbox3)
        pointCheckBoxes.append(vwCheckbox4)
        pointCheckBoxes.append(vwCheckbox5)
        pointCheckBoxes.append(vwCheckbox6)
        pointCheckBoxes.append(vwCheckbox7)
        pointCheckBoxes.append(vwCheckBox8)
        pointCheckBoxes.append(vwCheckBox9)
        pointCheckBoxes.append(vwCheckBox10)
        pointCheckBoxes.append(vwCheckBox11)
        pointCheckBoxes.append(vwCheckBox12)
        
        for checkbox in pointCheckBoxes {
            checkbox.image = #imageLiteral(resourceName: "bg_pscheckbox_unchecked")
        }
        
        vwGenGroupPointSys.isHidden = true
        
        typeLabels.append(lblType1)
        typeLabels.append(lblType2)
        typeLabels.append(lblType3)
        typeLabels.append(lblType4)
        
        for lable in typeLabels {
            lable.textColor = UIColor.black
        }
        
        cvSearchUserList.isHidden = true
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cvWidth = CGFloat(cvSearchUserList.frame.size.width)
        cvHeight = CGFloat(cvSearchUserList.frame.size.height)
        
        self.scrollView.contentSize = CGSize.init(width: self.scrollContentView.bounds.size.width, height: self.scrollContentView.bounds.size.height * 1.8)
    }

    //MARK:- Button Actions
    @IBAction func onMenu(_ sender:Any) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func onStartDate(_ sender: Any) {
        
        nSelectedDate = 1;
        vwDatePker.isHidden = false
    }
    
    @IBAction func onEndDate(_ sender: Any) {

        nSelectedDate = 2
        vwDatePker.isHidden = false
    }
    
    @IBAction func onSelectDate(_ sender: Any) {
        
        vwDatePker.isHidden = true
        if nSelectedDate == 2 {
            checkVaildDate()
        }
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        
        if nSelectedDate == 1 {
            tfStartDate.text = formatter.string(from: sender.date)
        } else {
            tfEndDate.text = formatter.string(from: sender.date)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField.text?.count != 0 {
            self.getUserInfoByName(name: textField.text!)
        }
    }
    
    func checkVaildDate() {
        let startDate = formatter.date(from: tfStartDate.text!)!
        let endDate = formatter.date(from: tfEndDate.text!)!
        
        if endDate < startDate {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_VAILD_DATE, positive: Const.OK, negative: nil)
            isValidDate = false
        }
        isValidDate = true
    }
    
    @IBAction func onChoosePrivateOrPublic(_ sender:Any) {
        
        let btn = sender as! UIButton
        nGroupMode = btn.tag
        
        if btn.tag == 0 {
            lblPrivateDesc.textColor = UIColor.red
            lblPublicDesc.textColor = UIColor.black            
            
        } else {
            lblPrivateDesc.textColor = UIColor.black
            lblPublicDesc.textColor = UIColor.red
            
        }
    }
    
    @IBAction func onNextInGroupName(_ sender:Any) {
        
        if tfChallengename.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.INPUT_CHALLANGENAME, positive: Const.OK, negative: nil)
            return
        } else if nGroupMode == -1 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_MODE, positive: Const.OK, negative: nil)
        } else if isValidDate {
            lblStepTitle.text = "Group Type"
            vwGroupname.isHidden = true
            vwType.isHidden = false
            vwPointSystem.isHidden = true
            vwCost.isHidden = true
            vwInviteMembers.isHidden = true
            lblStepTitle.text = "Group Type"
        } 
    }
    @IBAction func onTypeSelect(_ sender: Any) {
        
        let btn: UIButton = sender as! UIButton
        nGroupType = btn.tag
        
        for index in 0...typeLabels.count - 1 {
            if index == nGroupType {
                typeLabels[index].textColor = UIColor.red
            } else {
                typeLabels[index].textColor = UIColor.black
            }
        }
    }
    
    @IBAction func onPrevInType(_ sender:Any) {
        lblStepTitle.text = "Group name"
        vwGroupname.isHidden = false
        vwType.isHidden = true
        vwPointSystem.isHidden = true
        vwCost.isHidden = true
        vwInviteMembers.isHidden = true

    }

    @IBAction func onNextInType(_ sender:Any) {
        
        if nGroupType != -1 {
            lblStepTitle.text = "Point system"
            
            vwGroupname.isHidden = true
            vwType.isHidden = true
            vwPointSystem.isHidden = false
            vwCost.isHidden = true
            vwInviteMembers.isHidden = true
            if nGroupType == 0 {
               vwGenGroupPointSys.isHidden = false
            } else {
                vwGenGroupPointSys.isHidden = true
            }
        } else {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_TYPE, positive: Const.OK, negative: nil)
        }
    }
    
    @IBAction func onPrevInPointSystem(_ sender:Any) {
        
        lblStepTitle.text = "Group Type"
        
        vwGroupname.isHidden = true
        vwType.isHidden = false
        vwPointSystem.isHidden = true
        vwCost.isHidden = true
        vwInviteMembers.isHidden = true
    }
    
    @IBAction func onChoosePointSystem(_ sender:Any) {
        
        let btn = sender as! UIButton
        nSelectedPointSystem = btn.tag
        
        print("selected point system", btn.tag)
        
        /// if point system parameter is beachmark then show alert
        if nSelectedPointSystem == 5 {
            let alertController = UIAlertController(title: nil, message: Const.ALERT_WOD, preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: Const.YES, style: .default) { (action:UIAlertAction) in
               
                self.pointCheckBoxes[self.nSelectedPointSystem].image = #imageLiteral(resourceName: "bg_pscheckbox_checked")
                self.params[self.nSelectedPointSystem] = true
            }
            
            let action2 = UIAlertAction(title: Const.NO, style: .default) { (action:UIAlertAction) in
                
                self.pointCheckBoxes[self.nSelectedPointSystem].image = #imageLiteral(resourceName: "bg_pscheckbox_unchecked")
                self.params[self.nSelectedPointSystem] = false
                self.nSelectedPointSystem = -1
            }
            
            alertController.addAction(action1)
            alertController.addAction(action2)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            if params[nSelectedPointSystem] {
                pointCheckBoxes[nSelectedPointSystem].image = #imageLiteral(resourceName: "bg_pscheckbox_unchecked")
            } else {
                pointCheckBoxes[nSelectedPointSystem].image = #imageLiteral(resourceName: "bg_pscheckbox_checked")
            }
            params[nSelectedPointSystem] = !params[nSelectedPointSystem]
        }
    }
    
    @IBAction func onNextInPointSystem(_ sender:Any) {
        
        if nSelectedPointSystem != -1 {
            lblStepTitle.text = "Cost"
            vwGroupname.isHidden = true
            vwType.isHidden = true
            vwPointSystem.isHidden = true
            vwCost.isHidden = false
            vwInviteMembers.isHidden = true
        } else {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.SELECT_P_SYSTEM, positive: Const.OK, negative: nil)
        }
        
        var pCount = 0
        for index in 0...params.count - 1 {
            if params[index] {
                pointParameter += String(format: "param%d_", index+1)
                pCount = pCount+1
            }
        }
        
        if pCount > 1 {
            pointParameter = String(pointParameter[..<pointParameter.index(pointParameter.startIndex, offsetBy: pointParameter.count - 1)])
        }
    }
    
    @IBAction func onPrevInCost(_ sender:Any) {
        lblStepTitle.text = "Point system"
        vwGroupname.isHidden = true
        vwType.isHidden = true
        vwPointSystem.isHidden = false
        vwCost.isHidden = true
        vwInviteMembers.isHidden = true
        
        if nGroupType == 0 {
            vwGenGroupPointSys.isHidden = false
        } else {
            vwGenGroupPointSys.isHidden = true
        }
    }
    
    @IBAction func onNextInCost(_ sender:Any) {
        
        if tfCost.text?.count != 0 {
            
            onCreateGroup()
            
        } else {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.INPUT_COST, positive: Const.OK, negative: nil)
        }
    }
    
    @IBAction func onPrevInInviteMembers(_ sender:Any) {
        
        lblStepTitle.text = "Cost"
        
        vwGroupname.isHidden = true
        vwType.isHidden = true
        vwPointSystem.isHidden = true
        vwCost.isHidden = false
        vwInviteMembers.isHidden = true
    }
    
    @IBAction func onNextInInviteMembers(_ sender:Any) {
        
        self.lblStepTitle.text = "Group name"
        
        self.vwGroupname.isHidden = false
        self.vwType.isHidden = true
        self.vwPointSystem.isHidden = true;
        self.vwCost.isHidden = true
        self.vwInviteMembers.isHidden = true;
    }
    
    func showInviteMember() {
        
        lblStepTitle.text = "Invite members"
        lblTypeTitle.text = arrGroupTypeTitles[nGroupType]
        vwGroupname.isHidden = true
        vwType.isHidden = true
        vwPointSystem.isHidden = true
        vwCost.isHidden = true
        vwInviteMembers.isHidden = false
        
    }
    // MARK:  - UITextFieldDelegate methods

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        tfChallengename.resignFirstResponder()
        tfCost.resignFirstResponder()
        tfName.resignFirstResponder()
        tfEmail.resignFirstResponder()
        cvSearchUserList.isHidden = true
        return true
    }
    
    @IBAction func onAddMember(_ sender:Any) {
        
        tfName.resignFirstResponder()
        tfEmail.resignFirstResponder()
        if tfName.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.INPUT_NAME, positive: Const.OK, negative: nil)
        } else if tfEmail.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.INPUT_EMAIL, positive: Const.OK, negative: nil)
        } else if !CommonUtils.isValidEmail(tfEmail.text!) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.INPUT_VALIDEMAIL, positive: Const.OK, negative: nil)
        } else if inviteMemberId == currentUser!.userId {
            
            self.showToast(Const.MSG_ADD_YOURSELF)
            self.addMember()
            /*
            let alertController = UIAlertController(title: Const.APP_NAME, message: Const.MSG_ADD_YOURSELF, preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: Const.YES, style: .default) { (action:UIAlertAction) in
                
                self.addMember()
            }
            
            let action2 = UIAlertAction(title: Const.NO, style: .default) { (action:UIAlertAction) in
                alertController.dismiss(animated: true, completion: nil)
            }
           
            alertController.addAction(action1)
            alertController.addAction(action2)
            
            self.present(alertController, animated: true, completion: nil)
            */
        } else {
            /// add member API
            self.addMember()
        }
    }
    
    
    //MARK: -
    //MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - UITableViewDataSource methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInviteMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InviteMemberTableViewCell", for: indexPath) as! InviteMemberTableViewCell
        cell.selectionStyle = .none
        
        let member = arrInviteMembers[indexPath.row]
        
        
        cell.lblName.text = member.name
        cell.lblEmail.text = member.emaill        
        
        return cell
    }
    
    @IBAction func onSubmitGroup(_ sender:Any) {
    
        //onCreateGroup()
        if arrInviteMembers.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.INVALID_SUBMIT, positive: Const.OK, negative: nil)
        } else {
            //TODO:
            
            let alertController = UIAlertController(title: Const.APP_NAME, message: Const.MSG_CONGRATULATION_GROUP + tfStartDate.text!, preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: Const.OK, style: .default) { (action:UIAlertAction) in
                
                self.initGroup()
            }
            
            alertController.addAction(action1)
            
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func getUserInfoByName(name: String) {
        
        self.arrSearchUsers.removeAll()
        
        ApiRequest.getUserInfoByName(userName: name) { (resultCode, searchUserList) in
            
            if resultCode == Const.CODE_SUCCESS {
                self.arrSearchUsers = searchUserList
                if self.arrSearchUsers.count > 0 {
                    self.cvSearchUserList.isHidden = false
                } else {
                    self.cvSearchUserList.isHidden = true
                }
                 self.cvSearchUserList.reloadData()
            }
        }
        
    }
    
    func onCreateGroup() {
    
        /// do createGroup
        let group = GroupModel()
        group.groupName = tfChallengename.text!
        group.creatorName = (currentUser?.userName)!
        group.startDate = tfStartDate.text!
        group.endDate = tfEndDate.text!
        group.kindOfGroup = nGroupMode == 0 ? Const.KEY_PRIVATE : Const.KEY_PUBLIC
        group.groupType = arrGroupTypeTitles[nGroupType]
        group.pointParams = pointParameter
        group.price = Float(tfCost.text!)!
        
        self.showLoadingView()
        
        ApiRequest.createGroup(userId: (currentUser?.userId)!, group: group) { (resultCode, groupId) in
       
            self.hideLoadingView()
    
            if resultCode == Const.CODE_SUCCESS {
                self.createdGroupId = groupId
                self.showInviteMember()                
            } else if resultCode == Const.CODE_GROUP_EXIST {
                self.createdGroupId = groupId
                self.showAlertDialog(title: Const.APP_NAME, message: Const.EXIST_GROUP, positive: Const.OK, negative: nil)
                self.showInviteMember()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func addMember() {
        
        self.showLoadingView()
        
        print("invite id", self.inviteMemberId)
        
        ApiRequest.addMember(userId: inviteMemberId, creatorName: currentUser!.userName, email: tfEmail.text!, groupId: self.createdGroupId) { (resultCode) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {
                
                self.showToast(Const.MEMBER_ADDED)
                self.addMemberView()
            } else if resultCode == Const.CODE_UNREGISTERED_USER {
                self.showToast(Const.INVITATION_SENT)
                self.addMemberView()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
            
            /*else {
                
                let alertController = UIAlertController(title: Const.APP_NAME, message: Const.NON_EXIST_MEMBER, preferredStyle: .alert)
                
                let action1 = UIAlertAction(title: Const.YES_EMAIL, style: .default) { (action:UIAlertAction) in
                    print("You've pressed mail");
                    self.sendInviteMail()
                }
                
                let action2 = UIAlertAction(title: Const.YES_PHONE, style: .default) { (action:UIAlertAction) in
                    print("You've pressed phone");
                    self.sendInvitePhone()
                }
                
                let action3 = UIAlertAction(title: Const.NO, style: .default) { (action:UIAlertAction) in
                    print("You've pressed cancel");
                }
                
                alertController.addAction(action1)
                alertController.addAction(action2)
                alertController.addAction(action3)
                
                self.present(alertController, animated: true, completion: nil)
            }*/
        }
    }
    

    
    // MARK:- add member in list
    func addMemberView() {
        
        let inviteMemeber = InviteModel();
        inviteMemeber.name = tfName.text!
        inviteMemeber.emaill = tfEmail.text!
        
        arrInviteMembers.append(inviteMemeber)
        self.tblInviteMembers.reloadData()
        
//        vwInviteMembersScroll.contentSize = CGSize.init(width: 257, height: 47 * arrInviteMembers.count)
//        tblInviteMembers.frame = CGRect.init(x: 0, y: 0, width: 257, height: 47 * arrInviteMembers.count)
        
        tfName.text = ""
        tfEmail.text = ""
        inviteMemberId = -1
    }
    
    func initGroup() {
        
        initView()
        
        tfChallengename.text = ""
        tfName.text = ""
        tfEmail.text = ""
        tfCost.text = ""
        
        lblStepTitle.text = "Group name"
        vwGroupname.isHidden = false
        vwType.isHidden = true
        vwPointSystem.isHidden = true
        vwCost.isHidden = true
        vwInviteMembers.isHidden = true
        lblPrivateDesc.textColor = UIColor.black
        lblPublicDesc.textColor = UIColor.black
    }

    //MARK:- CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrSearchUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: cvWidth, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchUserCell", for: indexPath) as! SearchUserCell
        let searchUserOne = arrSearchUsers[indexPath.row]
        cell.lblSearchUser.text = searchUserOne.userName + ", " + searchUserOne.email
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let userModel = arrSearchUsers[indexPath.row]
        tfName.text = userModel.userName
        tfEmail.text = userModel.email
        inviteMemberId = userModel.userId
        cvSearchUserList.isHidden = true
    }
}
