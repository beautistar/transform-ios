//
//  GroupDetailViewController.swift
//  Transform
//
//  Created by Yin on 15/12/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class GroupDetailViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblGroupDetail: UITableView!
    
    var width: CGFloat = 0
    var selectedGroup = GroupModel()
    var arrParams = [String]()
    var typeDesc = "";
    
    @IBOutlet weak var peoplesCollectionView: UICollectionView!
    
    var userlist = [UserModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblGroupDetail.rowHeight = UITableViewAutomaticDimension
        tblGroupDetail.estimatedRowHeight = 25
        
        arrParams.removeAll()
        arrParams = selectedGroup.pointParams.components(separatedBy: "_")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        width = collectionView.frame.size.width
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        if selectedGroup.kindOfGroup == Const.KEY_PUBLIC {
            lblTitle.text = "This is " + selectedGroup.kindOfGroup + " " + selectedGroup.groupName + "nutrition challenge, created by " + selectedGroup.creatorName
            
        } else {
            lblTitle.text = "You are invited to " + selectedGroup.groupName + " nutrition challenge, created by " + selectedGroup.creatorName
        }
        
        
        for index in 0...arrGroupTypeTitles.count-1 {
            if arrGroupTypeTitles[index] == selectedGroup.groupType {
                typeDesc = arrGroupTypes[index]
                break
            }
        }
        
        self.getUserListInGroup()
        
    }
    
    func getUserListInGroup() {
        
        showLoadingView()
        
        ApiRequest.getUserListInGroup(groupId: selectedGroup.id) { (resultCode, userList) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {

                self.userlist = userList
                self.peoplesCollectionView.reloadData()

            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    @IBAction func onJoinNow(_ sender: Any) {
        
        let cardVC = self.storyboard?.instantiateViewController(withIdentifier: "CardInfoViewController") as! CardInfoViewController
        cardVC.selectedGroup = self.selectedGroup
        self.navigationController?.pushViewController(cardVC, animated: true)        
    }
    
    @IBAction func onBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Group detail tableview delegate & datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5 + arrParams.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row >= 0 && indexPath.row < 4 || indexPath.row == 4 + arrParams.count {
            // if cell is for show group common information
            let cell = UITableViewCell.init(style: .default, reuseIdentifier: "DetailCell")
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.font = UIFont(name: "Eras light ITC", size: 13)
            
            switch indexPath.row {
                
            case 0: cell.textLabel?.text = "Type of challenge: " + typeDesc
            case 1: cell.textLabel?.text = "Start date: " + selectedGroup.startDate
            case 2: cell.textLabel?.text = "End date: " + selectedGroup.endDate
            case 3: cell.textLabel?.text = "Point parameters:"
            default:
                cell.textLabel?.text = "Cost to join: \(selectedGroup.price)"
            }
            
            return cell
        } else /*if indexPath.row < 4+arrParams.count*/ {
            
            // for show point parameters
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PointParamsCell") as! PointParamsCell
            cell.setCell(status: true)
            print("arrParam: ---", arrParams[indexPath.row-4])
            switch arrParams[indexPath.row-4] {
                
            case Const.KEY_PARAM1:
                cell.lblParam.text = arrPointParmams[0]; break
            case Const.KEY_PARAM2:
                cell.lblParam.text = arrPointParmams[1]; break
            case Const.KEY_PARAM3:
                cell.lblParam.text = arrPointParmams[2]; break
            case Const.KEY_PARAM4:
                cell.lblParam.text = arrPointParmams[3]; break
            case Const.KEY_PARAM5:
                cell.lblParam.text = arrPointParmams[4]; break
            case Const.KEY_PARAM6:
                cell.setCell(status: true)
                cell.lblParam.text = arrPointParmams[5]; break
            case Const.KEY_PARAM7:
                cell.lblParam.text = arrPointParmams[6]; break
            case Const.KEY_PARAM8:
                cell.lblParam.text = arrPointParmams[7]; break
            case Const.KEY_PARAM9:
                cell.lblParam.text = arrPointParmams[8]; break
            case Const.KEY_PARAM10:
                cell.lblParam.text = arrPointParmams[9]; break
            case Const.KEY_PARAM11:
                cell.lblParam.text = arrPointParmams[10]; break
            case Const.KEY_PARAM12:
                cell.lblParam.text = arrPointParmams[11]; break
            default: break
            
            
            }
            
            return cell
        }
    }
    
    //MARK:- CollectionViewDelegate & Datasource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userlist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PublicGroupCell", for: indexPath) as! PublicGroupCell
        
        cell.lblName.text = userlist[indexPath.row].userName
        cell.lblStatus.text = userlist[indexPath.row].groupStatusIn
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize.init(width: width, height: 30.0)
    }
}
