//
//  HIWViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit
import AVKit

class HIWViewController: BaseViewController, SlideNavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource  {
    
    var helpList = [LibraryModel]()

    @IBOutlet weak var tblHelp: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblHelp.rowHeight = UITableViewAutomaticDimension
        tblHelp.estimatedRowHeight = 300
        
        tblHelp.tableFooterView = UIView()
        
        getHelp()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onMenu(_ sender : Any) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    func getHelp() {
        
        self.showLoadingView()
        
        ApiRequest.getHelp { (resCode, helpList) in
            
            self.hideLoadingView()
            
            if resCode == Const.CODE_SUCCESS {
                
                self.helpList = helpList
                self.tblHelp.reloadData()
                
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
            
        }
    }
    
    // MARK:- tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LibraryCell") as! LibraryCell
        
        let lib = helpList[indexPath.row]
        
        cell.lblTitle.text = lib.title
        cell.imvPicture.sd_setImage(with: URL(string: lib.preview_picture), placeholderImage: #imageLiteral(resourceName: "man"))
        cell.txvDescription.text = lib.description
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return UITableViewAutomaticDimension
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let fileURL = helpList[indexPath.row].file_url
        
        if fileURL.range(of:".pdf") != nil || fileURL.range(of:".PDF") != nil {
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "LibDetailViewController") as! LibDetailViewController
            detailVC.file_url = fileURL
            self.navigationController?.present(detailVC, animated: true, completion: nil)
        } else {
            let videoURL = URL(string: fileURL)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }

        /*
        let dController = UIDocumentInteractionController(url: URL(fileURLWithPath: helpList[indexPath.row].file_url))
        dController.presentOptionsMenu(from: self.view.bounds, in:self.view, animated:true)
        */
    }
}
