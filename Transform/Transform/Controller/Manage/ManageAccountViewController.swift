//
//  ManageAccountViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class ManageAccountViewController: UIViewController, SlideNavigationControllerDelegate, UITextFieldDelegate {
    
    
    @IBOutlet weak var tfCardnumber: UITextField!
    @IBOutlet weak var tfEmailid: UITextField!
    @IBOutlet weak var tfPhonenumber: UITextField!
    


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onMenu(_ sender:Any) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    
    // MARK: -UITextFieldDelegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfCardnumber.resignFirstResponder()
        tfEmailid.resignFirstResponder()
        tfPhonenumber.resignFirstResponder()
        
        return true
    }
}
