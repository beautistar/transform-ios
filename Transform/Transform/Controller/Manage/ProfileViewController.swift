//
//  ProfileViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController, SlideNavigationControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate ,UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblBodyWeight: UILabel!
    @IBOutlet weak var lblActivityFactor: UILabel!
    @IBOutlet weak var lblMeasurement1: UILabel!
    @IBOutlet weak var lblMeasurement2: UILabel!
    @IBOutlet weak var lblBodyFat: UILabel!
    @IBOutlet weak var lblBodyMass: UILabel!
    @IBOutlet weak var lblProtein: UILabel!
    @IBOutlet weak var lblCarb: UILabel!
    @IBOutlet weak var lblFat: UILabel!
    @IBOutlet weak var lblTotalBlocks: UILabel!
    @IBOutlet weak var tblPrevChallenge: UITableView!
    @IBOutlet weak var cvPreviousChallenge: UICollectionView!
    
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgUrl: String = ""
    var macro = MacroModel()
    var isCompletedProfile = false
    var challengeList = [RankingModel]()
    var prevChallenges = [GroupModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self._picker.delegate = self
        _picker.allowsEditing = true
        
        tblPrevChallenge.tableFooterView = UIView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imvProfile.sd_setImage(with: URL(string: currentUser!.photoUrl), placeholderImage: #imageLiteral(resourceName: "img_user"))
        imvProfile.layer.cornerRadius = imvProfile.frame.size.width / 2.0
        imvProfile.layer.masksToBounds = true
        
        initData()
    }
    
    func initData() {
        
        self.showLoadingView()
        
        lblName.text = "Name : " + currentUser!.userName
        lblGender.text = "Gender : " + currentUser!.property.gender
        lblAge.text = "Age : \(currentUser!.property.age)"
        lblBodyWeight.text = "Body weight : \(currentUser!.property.bodyWeight) lbs"
        lblActivityFactor.text = "Physical activity factor : " + currentUser!.property.activityFactor
        
        var bodyFat: Float = 0
        var bodyMass: Float = 0
        
        if currentUser!.property.gender == "Male" {
            lblMeasurement1.text = "Average wrist : \(currentUser!.property.wristMeasurement)"
            lblMeasurement2.text = "Average waist : \(currentUser!.property.waistMeasurement)"
            
            if currentUser!.property.wristMeasurement > 0 && currentUser!.property.waistMeasurement > 0 {
                bodyFat = Calculation.maleBodyFat()
                bodyMass = Calculation.maleBodyMass()
                isCompletedProfile = true
            } else {
                self.showToast("Profile did not setup")
            }
            
        } else {
            
            lblMeasurement1.text = "Average hip : \(currentUser!.property.averageHip)"
            lblMeasurement2.text = "Average Abdomen : \(currentUser!.property.averageAbdomen)"
            
            if currentUser!.property.averageHip > 0 && currentUser!.property.averageAbdomen > 0 {
                bodyFat = Calculation.femaleBodyFat()
                bodyMass = Calculation.femaleBodyMass()
                isCompletedProfile = true
            }
        }
        
        lblBodyFat.text = String(format: "Body fat : %.2f", bodyFat)
        lblBodyMass.text = String(format: "Body mass : %.2f", bodyMass)
        
        /// Point calculation with xlsx file
        let dailyProtein = Calculation.dailyProtein(bodyMass)
        lblProtein.text = String(format: "Protein : %.2f", dailyProtein)
        
        let totalBolcks = Calculation.totalBlocks(dailyProtein)
        lblTotalBlocks.text = String(format: "Total blocks : %.f", totalBolcks)
        
        let carb = Calculation.carb(totalBolcks)
        lblCarb.text = String(format: "Carbohydrates : %.2f", carb)
        
        let dailyFat = Calculation.dailyFat(totalBolcks)
        lblFat.text = String(format: "Fat : %.2f", dailyFat)
        
        macro.bodyFat = bodyFat
        macro.bodyMass = bodyMass
        macro.dailyProtein = dailyProtein
        macro.dailyCarb = carb
        macro.dailyFat = dailyFat
        macro.totalBlocks = Int(totalBolcks)
        macro.startingBodyFatPercent = currentUser!.macro.startingBodyFatPercent == 0 ? bodyFat : currentUser!.macro.startingBodyFatPercent
                
        updateMacro()
    }
    
    func updateMacro() {
        
        ApiRequest.updateMacroPoint(userId: currentUser!.userId, macroOne: macro) { (resultCode) in
            
            self.hideLoadingView()
            if resultCode == Const.CODE_SUCCESS {
                currentUser?.macro = self.macro
                self.getPrevChallenges()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getPrevChallenges() {
        
        ApiRequest.getPrevGroupList(userId: currentUser!.userId) { (resCode, prevChallenges) in
            
            if resCode == Const.CODE_SUCCESS {
                self.prevChallenges = prevChallenges
                self.cvPreviousChallenge.reloadData()
            }
        }
        
    }
    
    @IBAction func onMenu(_ sender:Any) {
        
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func onEditProfile(_ sender:Any) {
        
        let editCtlr = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        self.navigationController?.pushViewController(editCtlr, animated: true)
    }
    
    @IBAction func onAttach(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            && UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let galleryAction: UIAlertAction = UIAlertAction(title: Const.FROM_GALLERY, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let cameraAction: UIAlertAction = UIAlertAction.init(title: Const.FROM_CAMERA, style: UIAlertActionStyle.default, handler: { (cameraSourceAlert) in
                self._picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self._picker, animated: true, completion: nil)
            })
            
            
            photoSourceAlert.addAction(galleryAction)
            photoSourceAlert.addAction(cameraAction)
            photoSourceAlert.addAction(UIAlertAction(title: Const.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            self.imvProfile.image = pickedImage
            _imgUrl = saveToFile(image: pickedImage, filePath: Const.SAVE_ROOT_PATH, fileName: "profile.png")
            
            self.showLoadingView()
            
            ApiRequest.uploadPhoto(_imgUrl, userId: (currentUser?.userId)!, completion: { (resultCode, photoUrl) in
                
                self.hideLoadingView()
                
                if resultCode == Const.CODE_SUCCESS {
                    currentUser?.photoUrl = photoUrl
                    self.imvProfile.sd_setImage(with: URL(string: photoUrl), placeholderImage: #imageLiteral(resourceName: "img_user"))
                } else {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
                }
            })
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - RankingList TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return rankingUsers.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RankingListCell") as! RankingListCell
        
        let rankingUser = rankingUsers[indexPath.row]
        cell.imvProfile.sd_setImage(with: URL(string:  rankingUser.photoUrl), placeholderImage:#imageLiteral(resourceName: "img_user"))
        cell.lblRanking.text = "Ranking : " + "\(indexPath.row + 1)"
        cell.lblName.text = "User name : " + rankingUser.userName
        cell.lblPoint.text = "Total point : \(rankingUser.totalPoints)"
        
        if rankingUser.userId == currentUser!.userId {
            cell.contentView.layer.borderColor = UIColor.red.cgColor
            cell.contentView.layer.borderWidth = 0.5            
        } else {
            cell.backgroundColor = UIColor.white
        }
        return cell
    }
    
    //MARK: - PreviousChallenge CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return prevChallenges.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let challenge = prevChallenges[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchUserCell", for: indexPath) as! SearchUserCell
        cell.lblSearchUser.text = challenge.groupName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let rankingVC = self.storyboard?.instantiateViewController(withIdentifier: "RankingViewController") as! RankingViewController
        rankingVC.selectedGroup = prevChallenges[indexPath.row]
        self.navigationController?.pushViewController(rankingVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: cvPreviousChallenge.frame.size.width, height: 35)
    }
    
    
    
}











