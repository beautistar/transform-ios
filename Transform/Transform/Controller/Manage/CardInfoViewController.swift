//
//  CardInfoViewController.swift
//  Transform
//
//  Created by Yin on 31/01/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
//import AcceptSDK
import Stripe


class CardInfoViewController: BaseViewController {
    
    /// Merchant
    var kClientName =  "5nu7PFT82B6"
    var kClientKey =  "26x856Xfg6JprywPfz3kbxZDc8q727B4R9s7BJ86q4ccL2C3DGWQw88qZLJgsQXY"
    
    /// Sandbox
    // var kClientName = "5X7CwC2aFdhj"
    // var kClientKey = "4gmzJ9SyfbUdyxA37Rd3X2M5CzJy3QVk9pq6mbqwnThfDczJg5PnZc3Q8z62B4jk"

    
    @IBOutlet weak var getTokenButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    
    var selectedGroup = GroupModel()
    
    @IBOutlet weak var cardView: STPPaymentCardTextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getTokenButton.isHidden = true
        cardView.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func getTokenButtonTapped (_ sender: Any) {
        
        //activityIndicatorAcceptSDKDemo.startAnimating()
        
        self.showLoadingView()
        getToken()
    }
    
    
    func getToken() {
        
        view.endEditing(true)
        
        let handler = AcceptSDKHandler(environment: AcceptSDKEnvironment.ENV_LIVE)
        
        let request = AcceptSDKRequest()
        request.merchantAuthentication.name = kClientName
        request.merchantAuthentication.clientKey = kClientKey
        
        request.securePaymentContainerRequest.webCheckOutDataType.token.cardNumber = cardView.cardNumber!
        request.securePaymentContainerRequest.webCheckOutDataType.token.expirationMonth = "\(cardView.expirationMonth)"
        request.securePaymentContainerRequest.webCheckOutDataType.token.expirationYear = "\(cardView.expirationYear)"
        request.securePaymentContainerRequest.webCheckOutDataType.token.cardCode = cardView.cvc
        
        handler!.getTokenWithRequest(request, successHandler: { (inResponse:AcceptSDKTokenResponse) -> () in
            DispatchQueue.main.async {
                
                self.hideLoadingView()
                //self.activityIndicatorAcceptSDKDemo.stopAnimating()
                print("Token--->%@", inResponse.getOpaqueData().getDataValue())
                var output = String(format: "Response: %@\nData Value: %@ \nDescription: %@", inResponse.getMessages().getResultCode(), inResponse.getOpaqueData().getDataValue(), inResponse.getOpaqueData().getDataDescriptor())
                output = output + String(format: "\nMessage Code: %@\nMessage Text: %@", inResponse.getMessages().getMessages()[0].getCode(), inResponse.getMessages().getMessages()[0].getText())
                
                /// call payment process API
                
                let price = self.selectedGroup.price
                
                let expirationDate = "20" + "\(self.cardView.expirationYear)" + "-" + "\(self.cardView.expirationMonth)"
                
                self.showAlert(title: Const.APP_NAME, message: "Card validation is succeed", okButtonTitle: Const.OK, cancelButtonTitle: nil, okClosure: {
                    
                    self.showLoadingView()
                    ApiRequest.paymentProcess(groupId: self.selectedGroup.id, cardNumber: self.cardView.cardNumber!, expirationDate: expirationDate, cardCode: self.cardView.cvc!, amount: Float(price), completion: { (resultCode) in
                        
                        if resultCode == Const.CODE_SUCCESS {
                            self.joinGroup()
                        } else {
                            self.hideLoadingView()
                            self.showAlertDialog(title: Const.APP_NAME, message: Const.MSG_PAYMENT_FAILED, positive: Const.OK, negative: nil)
                            
                        }
                    })
                })
            }
        }) { (inError:AcceptSDKErrorResponse) -> () in
            self.hideLoadingView()
            let output = String(format: "Response:  %@\nError code: %@\nError text:   %@", inError.getMessages().getResultCode(), inError.getMessages().getMessages()[0].getCode(), inError.getMessages().getMessages()[0].getText())
            
            self.showAlertDialog(title: Const.APP_NAME, message: output, positive: Const.OK, negative: nil)
            print(output)
        }
    }
    
    func scrollTextViewToBottom(_ textView: UITextView) {
        if (textView.text.count > 0) {
            let bottom = NSMakeRange(textView.text.count-1, 1)
            textView.scrollRangeToVisible(bottom)
        }
    }
    
    @IBAction func hideKeyBoard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func joinGroup() {
        
        ApiRequest.joinGroup(userId: (currentUser?.userId)!, groupId: selectedGroup.id) { (resulrCode) in
            
            self.hideLoadingView()
            if resulrCode == Const.CODE_FAIL {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            } else if resulrCode == Const.CODE_SUCCESS {
                let successMsg = "Congratulations on joining the group Challenge, " + self.selectedGroup.groupName + "! You will now see the point parameters on your home page. Make sure you start logging your points starting on " + self.selectedGroup.startDate
                
                let alertController = UIAlertController(title: Const.APP_NAME, message: successMsg, preferredStyle: .alert)
                
                let action1 = UIAlertAction(title: Const.YES, style: .default) { (action:UIAlertAction) in
                    
                    self.navigationController?.popViewController(animated: true)
                }
                
                alertController.addAction(action1)
                
                self.present(alertController, animated: true, completion: nil)
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: "Payment failed. Please try again", positive: Const.OK, negative: nil)
            }
        }
    }
}

extension CardInfoViewController: STPPaymentCardTextFieldDelegate {
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if textField.isValid {
            getTokenButton.isHidden = false
        }
        else {
            getTokenButton.isHidden = true
        }
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        if textField.isValid {
            getTokenButton.isHidden = false
        }
        else {
            getTokenButton.isHidden = true
        }
    }
    
}
