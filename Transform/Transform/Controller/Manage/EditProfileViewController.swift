//
//  EditProfileViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var vwData: UIView!
    @IBOutlet weak var vwDataPicker: UIPickerView!
    
    @IBOutlet weak var vwBirthday: UIView!
    @IBOutlet weak var lblBirthday: UILabel!
    @IBOutlet weak var vwBirthdayDatePicker: UIDatePicker!
    @IBOutlet weak var lblAge: UILabel!
    
    @IBOutlet weak var tfBodyWeight: UITextField!
    @IBOutlet weak var lblMeasurementTitle1: UILabel!
    @IBOutlet weak var lblMeasurementTitle2: UILabel!
    @IBOutlet weak var tfMeasurement1: UITextField!
    @IBOutlet weak var tfMeasurement2: UITextField!
    @IBOutlet weak var tfHeight: UITextField!
    
    @IBOutlet weak var lblActivityFactor: UILabel!
    @IBOutlet weak var btnGender: UILabel!
    

    var nMode: nPickerMode = nPickerMode.gender
    var nGender = 0
    var nActivityFactor = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        lblGender.text = currentUser?.property.gender
       
        vwBirthdayDatePicker.date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        if let userBirthday = formatter.date(from: (currentUser?.property.birthday)!) {
            lblBirthday.text = formatter.string(from: userBirthday)
            let calendar = Calendar.current
            let component1 = calendar.dateComponents([.day, .month, .year, .hour], from: userBirthday)
            let component2 = calendar.dateComponents([.day, .month, .year], from: Date())
            
            lblAge.text = String(component2.year! - component1.year!)
        }
        tfBodyWeight.text = "\(currentUser!.property.bodyWeight)"
        if currentUser?.property.gender == "" {
            btnGender.isUserInteractionEnabled = true
        } else {
            btnGender.isUserInteractionEnabled = false
            nGender = currentUser!.property.gender == "Male" ? 0 : 1
        }
        
        
        if (nGender == 0) {
            self.lblMeasurementTitle1.text = "Wrist Measurement";
            self.lblMeasurementTitle2.text = "Waist Measurement";
            tfMeasurement1.text = "\(currentUser!.property.wristMeasurement)"
            tfMeasurement2.text = "\(currentUser!.property.waistMeasurement)"
        } else {
            self.lblMeasurementTitle1.text = "Hip Measurement";
            self.lblMeasurementTitle2.text = "Abdomen Measurement";
            tfMeasurement1.text = "\(currentUser!.property.averageHip)"
            tfMeasurement2.text = "\(currentUser!.property.averageAbdomen)"
        }
        tfHeight.text = "\(currentUser!.property.height)"

        for index in 0...arrActivityFactors.count - 1 {
            if currentUser!.property.activityFactor == arrActivityFactors[index] {
                nActivityFactor = index
                break
            }
        }
        lblActivityFactor.text = currentUser!.property.activityFactor
    }
    
    @IBAction func onBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onGender(_ sender: Any) {
        
        nMode = .gender
        self.vwData.isHidden = false
        self.vwDataPicker.reloadAllComponents()
        self.vwDataPicker.selectRow(nGender, inComponent: 0, animated: false)
    }
    
    @IBAction func onSelectGender (_ sender: Any) {
        
        vwData.isHidden = true
        let iRow = vwDataPicker.selectedRow(inComponent: 0)
        switch nMode {
        case .gender:
            nGender = vwDataPicker.selectedRow(inComponent: 0)
            lblGender.text = arrGenders[nGender]
            
            if nGender == 0 {
                self.lblMeasurementTitle1.text = "Wrist Measurement"
                self.lblMeasurementTitle2.text = "Waist Measurement"
            } else {
                self.lblMeasurementTitle1.text = "Hip Measurement"
                self.lblMeasurementTitle2.text = "Abdomen Measurement"
            }
            
        case .activityFactor:
            nActivityFactor = vwDataPicker.selectedRow(inComponent: 0)
            lblActivityFactor.text = arrActivityFactors[nActivityFactor]
            
        case .weight:
            tfBodyWeight.text = String(arrBodyWeight[iRow])
            
        case .height:
            tfHeight.text = String(arrHeight[iRow])
            
        case .measurement1:
            tfMeasurement1.text = nGender == 0 ? String(arrWrist[iRow]) : String(arrHips[iRow])
            
        case .measurement2:
            tfMeasurement2.text = nGender == 0 ? String(arrWaist[iRow]) : String(arrAbdomes[iRow])
        }
    }
    
    @IBAction func onActivtyFactor(_ sender: Any) {
        
        nMode = .activityFactor
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(nActivityFactor, inComponent: 0, animated: false)
    }
    
    @objc func loadActivityFactor() {
        
         vwDataPicker.isHidden = false
    }
    
    @IBAction func onWeight(_ sender: Any) {
        nMode = .weight
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(0, inComponent: 0, animated: false)
    }
    
    @IBAction func onHeight(_ sender: Any) {
        nMode = .height
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(0, inComponent: 0, animated: false)
    }
    
    @IBAction func onMeasurement1(_ sender: Any) {
        nMode = .measurement1
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(0, inComponent: 0, animated: false)
    
    }
    
    @IBAction func onMeasurement2(_ sender: Any) {
        nMode = .measurement2
        vwData.isHidden = false
        vwDataPicker.reloadAllComponents()
        vwDataPicker.selectRow(0, inComponent: 0, animated: false)
    }
    
    
    @IBAction func onBirthday(_ sender: Any) {
        
        vwBirthday.isHidden = false
    }
    
    @IBAction func onSelectBirthDate(_ sender: Any) {
        
        self.vwBirthday.isHidden = true
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        lblBirthday.text = dateFormatter.string(from: vwBirthdayDatePicker.date)
        let component1 = Calendar.current.dateComponents([.day, .month, .year, .hour], from: vwBirthdayDatePicker.date)
        let component2 = Calendar.current.dateComponents([.day, .month, .year], from: Date())
        lblAge.text = String(component2.year! - component1.year!)
    }
    
    func isValid() -> Bool {
        
        if Int(lblAge.text!)! < 1 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_VAILD_AGE, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfBodyWeight.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_BODY_WEIGHT_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfHeight.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_HEIGHT_EMPTY, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfMeasurement1.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: nGender == 0 ? Const.CHECK_WRIST_MEASUREMENT : Const.CHECK_HIP_MEASUREMENT, positive: Const.OK, negative: nil)
            return false
        }
        
        if tfMeasurement2.text?.count == 0 {
            self.showAlertDialog(title: Const.APP_NAME, message: nGender == 0 ? Const.CHECK_WAIST_MEASUREMENT : Const.CHECK_ABDMEN_MEASUREMENT, positive: Const.OK, negative: nil)
            return false
        }
        
        if nGender == 0 && (Float(tfMeasurement2.text!)! - Float(tfMeasurement1.text!)! < 22.0) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.CHECK_INVVALIDE_WARIST_WAIST, positive: Const.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    @IBAction func onSave(_ sender: Any) {
        
        if isValid() {
            
            /// upload prfile informatoin
            
            let property = PropertyModel()
            
            property.gender = lblGender.text!
            property.birthday = lblBirthday.text!
            property.age = Int(lblAge.text!)!
            property.height = Float(tfHeight.text!)!
            property.bodyWeight = Float(tfBodyWeight.text!)!
            property.activityFactor = lblActivityFactor.text!
            property.wristMeasurement = nGender == 0 ? Float(tfMeasurement1.text!)! : 0.0
            property.waistMeasurement = nGender == 0 ? Float(tfMeasurement2.text!)! : 0.0
            property.averageHip = nGender == 1 ? Float(tfMeasurement1.text!)! : 0.0
            property.averageAbdomen = nGender == 1 ? Float(tfMeasurement2.text!)! : 0.0
            property.startingBodyWeight = currentUser!.property.startingBodyWeight
            
            self.showLoadingView()
            ApiRequest.uploadProperty(userId: (currentUser?.userId)!, property: property, completion: { (resultCode)  in
                self.hideLoadingView()
                if resultCode == Const.CODE_SUCCESS {
                    self.navigationController?.popViewController(animated: true)
                    currentUser?.property = property
                    isCompletedProfile = true
                    SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
                } else {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
                }
            })
        }
    }
    
    //MARK: - UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch nMode {
        case .gender:
            return arrGenders.count
        case .weight:
            return arrBodyWeight.count
        case .height:
            return arrHeight.count
        case .activityFactor:
            return arrActivityFactors.count
        case .measurement1:
            return nGender == 0 ? arrWrist.count : arrHips.count
        case .measurement2:
            return nGender == 0 ? arrWaist.count : arrAbdomes.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch nMode {
        case .gender:
            return arrGenders[row]
        case .activityFactor:
            return arrActivityFactors[row]
        case .weight:
            return String(arrBodyWeight[row])
        case .height:
            return String(arrHeight[row])
        case .measurement1:
            return nGender == 0 ? String(arrWrist[row]) : String(arrHips[row])
        case .measurement2:
            return nGender == 0 ? String(arrWaist[row]) : String(arrAbdomes[row])
        }
    }
    
    //MARK: - UITextFieldDelegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        tfBodyWeight.resignFirstResponder()
        tfHeight.resignFirstResponder()
        tfMeasurement1.resignFirstResponder()
        tfMeasurement2.resignFirstResponder()
        return true
    }
}
