//
//  MenuViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit


class MenuViewController: UIViewController, SlideNavigationControllerDelegate {
    
    var slideOutAnimationEnabled:Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        slideOutAnimationEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func homeAction(_ sender: Any) {
        
        let homeCtlr = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: homeCtlr, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
    }
    
    @IBAction func howItWorkAction(_ sender: Any) {
        
        let hiwVC = self.storyboard?.instantiateViewController(withIdentifier: "HIWViewController") as! HIWViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: hiwVC, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
    }
    
    @IBAction func libraryAction(_ sender: Any) {
        
        let libCtlr = self.storyboard?.instantiateViewController(withIdentifier: "LibraryViewController") as! LibraryViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: libCtlr, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
    }
    @IBAction func profileAction(_ sender: Any) {
        
        let proCtlr = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: proCtlr, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
    }
    @IBAction func createGroupAction(_ sender: Any) {
        
        let createCtlr = self.storyboard?.instantiateViewController(withIdentifier: "CreateGroupViewController") as! CreateGroupViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: createCtlr, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
    }
    @IBAction func joinGroupAction(_ sender: Any) {
        
        let joinCtlr = self.storyboard?.instantiateViewController(withIdentifier: "JoinGroupViewController") as! JoinGroupViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: joinCtlr, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
    }
    @IBAction func manageAccountAction(_ sender: Any) {
        let manageCtlr = self.storyboard?.instantiateViewController(withIdentifier: "ManageAccountViewController") as! ManageAccountViewController
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: manageCtlr, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
    }
    @IBAction func logoutAction(_ sender: Any) {
        ApiRequest.saveToken(token: "", completion: { (resultCode) in
        })
        isCompletedProfile = false
        SlideNavigationController.sharedInstance().popToRootViewController(animated: true)
    }

    @IBAction func fbTapped(_ sender: Any) {
        
        if let url = URL(string: Const.facebookURL) {
            UIApplication.shared.open(url, options: [:])
        }
        
        SlideNavigationController.sharedInstance().closeMenu(completion: nil)
    }
    
    @IBAction func instaTapped(_ sender: Any) {
        
        if let url = URL(string: Const.instagramURL) {
            UIApplication.shared.open(url, options: [:])
        }
        
        SlideNavigationController.sharedInstance().closeMenu(completion: nil)
    }
    // MARK: - SlideNavigationController Methods
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        return true
    }
}
