//
//  ChattingViewController.swift
//  Transform
//
//  Created by Yin on 20/12/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import Alamofire
import SwiftyJSON
import MobileCoreServices
import SDWebImage


class ChattingViewController: JSQMessagesViewController {
    
    
    
    // MARK: Properties
    private let imageURLNotSetKey = "NOTSET"
    
    let _picker: UIImagePickerController = UIImagePickerController()
    
    private var _jsqMessages: [JSQMessage] = []
    private var _messages = [MessageModel]()
    
    var _group = GroupModel()
    var _roomName = ""
    var _page = 0
    var _from = 0
    var _imgPath = ""
    var _videoPath = ""
    
    fileprivate var displayName: String!
    
    var outgoingBubble: JSQMessagesBubbleImage! // = self.setupOutgoingBubble()
    var incomingBubble: JSQMessagesBubbleImage! // = self.setupIncomingBubble()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        outgoingBubble = self.setupOutgoingBubble()
        incomingBubble = self.setupIncomingBubble()

        self.senderId = String(currentUser!.userId)
        //self.senderId = "\(currentUser!.userId)"
        self.senderDisplayName = currentUser?.userName
        self.title = _group.creatorName
        
        self.view.backgroundColor = UIColor.black
        
        self.collectionView.showsVerticalScrollIndicator = true

        let avatarSize = CGSize(width: 40, height: 40)

        collectionView!.collectionViewLayout.incomingAvatarViewSize = avatarSize
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = avatarSize
        collectionView!.collectionViewLayout.sectionInset = UIEdgeInsets(top: 44, left: 10, bottom: 0, right: 10)
        collectionView.backgroundColor = UIColor.clear

        // This is a beta feature that mostly works but to make things more stable it is diabled.
        collectionView?.collectionViewLayout.springinessEnabled = false
        automaticallyScrollsToMostRecentMessage = true
        self.collectionView?.reloadData()
        self.collectionView?.layoutIfNeeded()
        self.inputToolbar.contentView?.leftBarButtonItem = nil;
        
        ///  add custom view at top
        
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 64))
        view.backgroundColor = Const.lightBlackColor
        
        let backButton = UIButton.init(frame: CGRect.init(x: 0, y: 20, width: 64, height: 44))
        backButton.setTitle("Back", for: .normal)
        backButton.addTarget(self, action: #selector(didTapBack), for: UIControlEvents.touchUpInside)
        backButton.setTitleColor(UIColor.white, for: .normal)
        backButton.titleLabel?.font = UIFont.init(name: "Eras Light ITC", size: 15)
        
        let titleLabel = UILabel.init(frame: CGRect.init(x: -20, y: 20, width: self.view.frame.size.width, height: 44))
        titleLabel.text = _group.groupName
        titleLabel.textColor = UIColor.white
        titleLabel.textAlignment = NSTextAlignment.center
       
        view.addSubview(titleLabel)
        view.addSubview(backButton)
        self.view.addSubview(view)
        
        self.getMessage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        g_currentVC = self
    }

    func getMessage() {
        
        _jsqMessages.removeAll()
        _messages.removeAll()
        
        showLoadingView()
        
        ApiRequest.getMessages(groupId: _group.id) { (resultCode, messages, jsqMessages) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {
                self._jsqMessages = jsqMessages
                self._messages = messages
                
                self.finishReceivingMessage(animated: true)
                self.hideLoadingView()
                
                if self._messages.count > 0 {
                    self.showAlertDialog(title: Const.APP_NAME, message: Const.BLOCK_DESCRIPTION, positive: Const.OK, negative: nil)
                }                
                
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }        
    }
    
    func sendMessage(text:String) {
        
        ApiRequest.sendMessages(userId: (currentUser?.userId)!, groupId: _group.id, message: text) { (resultCode) in           

            if resultCode != Const.CODE_SUCCESS {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    @objc func didTapBack() {
       
       self.navigationController?.popViewController(animated: true)
    }
    
    @objc func goBack() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - JSQ delegate
    // MARK: Collection view data source (and related) methods
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _jsqMessages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        
        return _jsqMessages[indexPath.row]
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        
        print("self.senderId = \(self.senderId), message sender id = \(_jsqMessages[indexPath.row].senderId)")
        
        let messageSenderId = _jsqMessages[indexPath.row].senderId
        return (messageSenderId == self.senderId) ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        
        let message = _jsqMessages[indexPath.item]
        
        cell.textView?.textColor = UIColor.white
        cell.cellBottomLabel.textColor = UIColor.lightText
        cell.cellBottomLabel.text = message.senderDisplayName
        cell.avatarImageView.sd_setImage(with: URL(string:  _messages[indexPath.item].photoUrl), placeholderImage:#imageLiteral(resourceName: "img_user"))
        
        cell.avatarImageView.layer.borderWidth = 2.0
        cell.avatarImageView.layer.cornerRadius = 20
        cell.avatarImageView.layer.masksToBounds = true
        cell.avatarImageView.layer.borderColor = UIColor.white.cgColor
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 12
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return 24
    }
   
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        self.addMessage(withId: senderId, name: senderDisplayName, text: text)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        sendMessage(text: text)
        
        let msg = MessageModel()
        msg.userId = (currentUser?.userId)!
        msg.userName = (currentUser?.userName)!
        msg.photoUrl = (currentUser?.photoUrl)!
        msg.messageContent = text

        _messages.append(msg)
        finishSendingMessage()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapCellAt indexPath: IndexPath!, touchLocation: CGPoint) {
        reportUserComment(indexPath.row)
    }

    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapAvatarImageView avatarImageView: UIImageView!, at indexPath: IndexPath!) {
        reportUserComment(indexPath.row)
    }
    
    
    func reportUserComment(_ index: Int) {
        
        let actionAlert: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let blockAction: UIAlertAction = UIAlertAction(title: Const.STR_BLOCK, style: UIAlertActionStyle.destructive, handler: {
            (photoSourceAlert) -> Void in
            
            
            self.showAlert(title: Const.APP_NAME, message: Const.BLOCK_CONFIRM, okButtonTitle: Const.YES, cancelButtonTitle: Const.NO, okClosure: {
                
                self.showLoadingView()
                
                let msg = self._messages[index]
                msg.messageContent = Const.BLOCK_MSG
                
                let newMessage : JSQMessage!
                
                newMessage = JSQMessage(senderId: "\(msg.userId)", displayName: msg.userName,  text: msg.messageContent.decodeEmoji);
                self._jsqMessages.remove(at: index)
                self._jsqMessages.insert(newMessage, at: index)
                
                ApiRequest.blockMessage(msg.messageId, messageContent: Const.BLOCK_MSG, completion: { (resCode) in
                    
                    self.hideLoadingView()
                    
                    if resCode != Const.CODE_SUCCESS {
                        self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
                    } else {
                        self.collectionView.reloadData()
                    }
                })
            })
        })
        
        actionAlert.addAction(blockAction)

        actionAlert.addAction(UIAlertAction(title: Const.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(actionAlert, animated: true, completion: nil);
    }
    
    ///---------
    
    func onReceiveMessage(message:MessageModel) {
        
        self.addMessage(withId: "\(message.userId)", name: "\(message.userName)", text: message.messageContent)
        _messages.append(message)
         self.finishReceivingMessage()
    }
    
    // MARK: UI and User Interaction
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text.decodeEmoji) {
            _jsqMessages.append(message)
        }
    }
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: Const.lightBlackColor);
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: Const.lightBlackColor)
    }
    
}

extension ChattingViewController {
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func showLoadingView() {
        
        showLoadingViewWithTitle(title: "")
    }
    
    
    func showLoadingViewWithTitle(title: String) {
        
        if title == "" {
            
            ProgressHUD.show()
            
        } else {
            
            ProgressHUD.showWithStatus(string: title)
        }
    }
    
    // hide loading view
    func hideLoadingView() {
        
        ProgressHUD.dismiss()
    }
    
    internal func showAlert(title: String?, message: String?, okButtonTitle: String, cancelButtonTitle: String?, okClosure: (() -> Void)?) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let yesAction = UIAlertAction(title: okButtonTitle, style: .default, handler: { (action: UIAlertAction) in
            
            if okClosure != nil {
                okClosure!()
            }
        })
        
        alertController.addAction(yesAction)
        if cancelButtonTitle != nil {
            
            let noAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { (action: UIAlertAction) in
                
            })
            alertController.addAction(noAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
}
