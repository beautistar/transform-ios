//
//  GroupMembersViewController.swift
//  Transform
//
//  Created by Yin on 28/01/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit
import Presentr

class GroupMembersViewController: BaseViewController,  UITableViewDelegate, UITableViewDataSource, PresentrDelegate {
    
    var memberList = [RankingModel]()
    var selectedGroup = GroupModel()
    var isBenchMarked = false
    var selMember = RankingModel()
    var dismissHandler: (() -> Void)?
    
    var selectedIndexPath: IndexPath?
    
    @IBOutlet weak var tblMemberList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblMemberList.tableFooterView = UIView()
        
        getRankingList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func getRankingList() {
        
        self.showLoadingView()
        
        memberList.removeAll()
        rankingUsers.removeAll()
        
        ApiRequest.getRankingList(groupId: (selectedGroup.id)) { (resultCode, rankingList) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {
                
                rankingUsers = rankingList
                self.memberList = rankingList
                
                self.tblMemberList.reloadData()
                
            } else {
                self.hideLoadingView()
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: dismissHandler)
    }
    
    //MARK:- Group Member list collectionn view
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RankingListCell") as! RankingListCell
        
        let member = memberList[indexPath.row]
        cell.imvProfile.sd_setImage(with: URL(string:  member.photoUrl), placeholderImage:#imageLiteral(resourceName: "img_user"))
        cell.lblName.text = "User name : " + member.userName
        cell.lblPoint.text = "Total point : \(member.totalPoints)"
        
        if isBenchMarked {
            if member.benchmark == 1 {
                cell.imvCheck.isHidden = false
            } else {
                cell.imvCheck.isHidden = true
            }
        } else {
            if selectedIndexPath == indexPath {
                cell.imvCheck.isHidden = false
            } else {
                cell.imvCheck.isHidden = true
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if selectedGroup.challengeFinished == 1 {
            return
        }
        
        selMember = memberList[indexPath.row]
        
        if !isBenchMarked {
            
            selectedIndexPath = indexPath
            tblMemberList.reloadData()            
        }
    }
    
    @IBAction func giveBenchmark(_ sender: Any) {
        
        if selMember.userId == 0 || isBenchMarked {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.MSG_ALREADY_BEANCHMARK, positive: Const.OK, negative: nil)
            return
        }
       
        self.showLoadingView()
        
        ApiRequest.giveBenchmark(groupId: selectedGroup.id, userId: selMember.userId) { (resultCode) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {
                
                isGranted = true
                
                for member in self.memberList {
                    if member.userId == self.selMember.userId {
                        member.benchmark = 1
                        member.totalPoints += 5
                        self.isBenchMarked = true
                        break
                    }
                }
                
                self.tblMemberList.reloadData()
                
                
                
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        dismissHandler?()
        return true
    }

}

