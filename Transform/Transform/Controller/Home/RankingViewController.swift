//
//  RankingViewController.swift
//  Transform
//
//  Created by Yin on 03/01/2018.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

class RankingViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var rankingList = [RankingModel]()
    var selectedGroup = GroupModel()
    @IBOutlet weak var tblRankingList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblRankingList.tableFooterView = UIView()
        
        getRankingList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)        
    }
    
    @objc func getRankingList() {
        
        self.showLoadingView()
        
        rankingList.removeAll()
        rankingUsers.removeAll()
        
        ApiRequest.getRankingList(groupId: (selectedGroup.id)) { (resultCode, rankingList) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {
                
                rankingUsers = rankingList
                self.rankingList = rankingList
                
                self.tblRankingList.reloadData()
                
            } else {
                self.hideLoadingView()
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    //MARK: - TableView datasource & delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rankingList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RankingListCell") as! RankingListCell
        let rankingUser = rankingList[indexPath.row]
        cell.imvProfile.sd_setImage(with: URL(string:  rankingUser.photoUrl), placeholderImage:#imageLiteral(resourceName: "img_user"))
        cell.lblRanking.text = "Ranking : " + "\(indexPath.row + 1)"
        cell.lblName.text = "User name : " + rankingUser.userName
        cell.lblPoint.text = "Total point : \(rankingUser.totalPoints)"
        
        if rankingUser.userId == currentUser!.userId {
            cell.contentView.layer.borderColor = UIColor.red.cgColor
            cell.contentView.layer.borderWidth = 0.5
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
}
