//
//  HomeViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit
import Presentr

class HomeViewController: BaseViewController, SlideNavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource, PresentrDelegate {
    
    @IBOutlet weak var vwGroupSelect:UIView!
    @IBOutlet weak var vwGroupPicker:UIPickerView!
    @IBOutlet weak var lblGroupName:UILabel!
    @IBOutlet weak var btnGroup:UIButton!
    @IBOutlet weak var vwContent:UIView!
    @IBOutlet weak var vwGroupAction:UIView!
    
    @IBOutlet weak var lblTotalPoint: UILabel!
    @IBOutlet weak var lblTodayPoint: UILabel!
    @IBOutlet weak var lblCurrentPlace: UILabel!
    @IBOutlet weak var lblStartingBodyWeight: UILabel!
    @IBOutlet weak var lblStartingFat: UILabel!
    @IBOutlet weak var tblPointParams: UITableView!
    
    @IBOutlet weak var lblEatenBlockPercent: UILabel!
    @IBOutlet weak var lblEatenProtein: UILabel!
    @IBOutlet weak var lblEatenCarb: UILabel!
    @IBOutlet weak var lblEatenFat: UILabel!
    
    @IBOutlet weak var lblNeededProtein: UILabel!
    @IBOutlet weak var lblNeededCarb: UILabel!
    @IBOutlet weak var lblNeededFat: UILabel!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var vwMemberList: UIView!
    
    var arrGroups = [GroupModel]()
    var selectedGroup = GroupModel()
    var arrParams = [String]()
    var eatenBlocks: Int = 0 
    var haveBenchmark: Bool = false ///  this means selected group have benchmark point parameter
    var isBenchMarked: Bool = false /// This is if group member already have get benchmarked or not
    var cvWidth: CGFloat = 0.0
    
    var arrGroupMembers = [RankingModel]()
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .popup)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        
        return presenter
    }()
    

    
    var checkedParams = [Bool]()
    var todayDate = ""
    var pointCount: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        g_currentVC = self
        cvWidth = vwMemberList.frame.size.width
        self.scrollView.contentSize = CGSize.init(width: self.vwContent.bounds.size.width, height: self.vwContent.bounds.size.height)
        
        perform(#selector(loadGroups), with: nil, afterDelay: 0.5)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("didload")
    }
    
    func initView() {
        
        vwContent.isHidden = true;
        vwGroupAction.isHidden = true
        lblGroupName.isHidden = true
        btnGroup.isHidden = true
        
        tblPointParams.tableFooterView = UIView()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        todayDate = formatter.string(from: date)
        
        // manage checked point parameters
        checkedParams.removeAll()
        if let _checkedParams = UserDefaults.standard.array(forKey: Const.KEY_CHECKED_PARAMS) {
            self.checkedParams = _checkedParams as! [Bool]
            
            // check if date is passed from last checked point date
            
            if let savedDate = UserDefaults.standard.string(forKey: Const.KEY_SAVEDDATE) {
                
                if savedDate != todayDate {
                    for index in 0...arrPointParmams.count-1 {
                        checkedParams[index] = false
                    }
                }
            }
            
        } else {
            for _ in 1...arrPointParmams.count {
                self.checkedParams.append(false)
            }
        }
    }
    
    func initData() {
        
        if (currentUser?.property.gender == arrGenders[0] && currentUser?.property.waistMeasurement == 0) || (currentUser?.property.gender == arrGenders[1] && currentUser?.property.averageHip == 0) {
            self.showAlertDialog(title: Const.APP_NAME, message: Const.COMPLETE_PROFILE, positive: Const.OK, negative: nil)
            return
        }
        
        lblStartingBodyWeight.text = "Starting Body weight(lbs) : \(currentUser!.property.startingBodyWeight)"
        lblStartingFat.text = "Starting Fat(%) : \(currentUser!.macro.startingBodyFatPercent)"
        
        updatePoints()
        
        eatenBlocks = currentUser!.macro.eatBlocksTillNow
        setBlockView()
        
        /// setup point parameters
        arrParams.removeAll()
        
        // add base point due to group type
        if selectedGroup.groupType == arrGroupTypeTitles[0] { // Health&Wellness
            arrParams.append(Const.KEY_PARAM_GENERAL_BASE)
        } else if selectedGroup.groupType == arrGroupTypeTitles[2] {// Paleo
            arrParams.append(Const.KEY_PARAM_PALEO_BASE)
        } else if selectedGroup.groupType == arrGroupTypeTitles[3] { // Keto
            arrParams.append(Const.KEY_PARAM_KETO_BASE)
        }
        
        for param in selectedGroup.pointParams.components(separatedBy: "_") {
            arrParams.append(param)
        }
        
        
        //check if selected group have benchmark option
        if arrParams.contains(Const.KEY_PARAM6) {
            haveBenchmark = true
        } else {
            haveBenchmark = false
        }
        
        /// show/hide groupmember button if selected group is owner.
        
        if selectedGroup.creatorId == currentUser!.userId {
            vwMemberList.isHidden = false
            vwMemberList.frame.size.height = 70
        } else {
            vwMemberList.isHidden = true
            vwMemberList.frame.size.height = 0
        }
        
        tblPointParams.frame.size.height = CGFloat(35 * arrParams.count)
        tblPointParams.reloadData()
        
        /// setup group member view size
        vwMemberList.frame.origin.y = tblPointParams.frame.origin.y + tblPointParams.frame.size.height + 10
        self.scrollView.contentSize = CGSize.init(width: self.vwContent.bounds.size.width-20, height: self.vwContent.bounds.size.height+vwMemberList.frame.size.height+tblPointParams.frame.size.height)
        scrollContentView.frame.size = CGSize.init(width: self.vwContent.bounds.size.width-20, height: self.vwContent.bounds.size.height+vwMemberList.frame.size.height+tblPointParams.frame.size.height)
        scrollContentView.layoutIfNeeded()
        
        
        /// check if challenge is finished
        
        if selectedGroup.challengeFinished == 1 {
            
            self.showAlertDialog(title: Const.APP_NAME, message: selectedGroup.groupName + " " + Const.MSG_FINISHEDGROUP, positive: Const.OK, negative: nil)
        }
    }
    
    func updatePoints() {
        
        for pointModel in currentUser!.points {
            if selectedGroup.id == pointModel.groupId {
                lblTotalPoint.text = "Total Points : " + "\(pointModel.totalPoints)"
                lblTodayPoint.text = "Today's points : " + "\(pointModel.todayPoints)"
                break
            } else {
                lblTotalPoint.text = "Total Points : "
                lblTodayPoint.text = "Today's points : "
            }
        }
    }
    
    func addPoint(point: Int) {
        
        if currentUser!.points.count == 0 {
            
            let pointModel = PointModel()
            pointModel.groupId = selectedGroup.id
            pointModel.todayPoints = 0
            pointModel.totalPoints = 0
            currentUser?.points.append(pointModel)
        }
        
        if currentUser!.points.count > 0 {
            for index in 0...currentUser!.points.count - 1 {
                let pointModel: PointModel = currentUser!.points[index]
                if selectedGroup.id == pointModel.groupId {
                    lblTotalPoint.text = "Total Points : " + "\(pointModel.totalPoints + point)"
                    lblTodayPoint.text = "Today's points : " + "\(pointModel.todayPoints + point)"
                    currentUser!.points[index].totalPoints += point
                    currentUser!.points[index].todayPoints += point
                    break
                } else {
                    let pointModel = PointModel()
                    pointModel.groupId = selectedGroup.id
                    pointModel.todayPoints = point
                    pointModel.totalPoints = point
                    
                    currentUser?.points.append(pointModel)
                }
            }
            
            updatePoints()
        }
    }
    
    
    func deletePoint(point: Int) {
        
        if currentUser!.points.count > 0 {
            for index in 0...currentUser!.points.count {
                let pointModel: PointModel = currentUser!.points[index]
                if selectedGroup.id == pointModel.groupId {
                    lblTotalPoint.text = "Total Points : " + "\(pointModel.totalPoints - point)"
                    lblTodayPoint.text = "Today's points : " + "\(pointModel.todayPoints - point)"
                    currentUser!.points[index].totalPoints -= point
                    currentUser!.points[index].todayPoints -= point
                    break
                }else {
                    let pointModel = PointModel()
                    pointModel.groupId = selectedGroup.id
                    pointModel.todayPoints = point
                    pointModel.totalPoints = point
                    
                    currentUser?.points.append(pointModel)
                }
            }
            
            updatePoints()
        }
    }
    
    @objc func loadGroups() {
        
        arrGroups.removeAll()
        
        showLoadingView()
        
        ApiRequest.getAllGroupList(userId: (currentUser?.userId)!) { (resultCode, groupList) in
            
            if resultCode == Const.CODE_SUCCESS {
                
                self.arrGroups = groupList
                
                if self.arrGroups.count > 0 {
                    self.selectedGroup = self.arrGroups[0]
                    self.vwGroupPicker.reloadAllComponents()
                    self.vwContent.isHidden = false
                    self.vwGroupAction.isHidden = false
                    self.lblGroupName.isHidden = false
                    self.btnGroup.isHidden = false
                    if self.arrGroups[0].creatorId == currentUser!.userId {
                        self.lblGroupName.text = self.arrGroups[0].groupName + " (My group)"
                    } else {
                        self.lblGroupName.text = self.arrGroups[0].groupName
                    }
                    
                } else {
                    self.vwContent.isHidden = false
                    self.vwGroupAction.isHidden = true
                    self.lblGroupName.isHidden = false
                    self.lblGroupName.text = "No groups"
                    self.btnGroup.isHidden = true
                }
                
                self.initData()
                self.perform(#selector(self.getRankingList), with: nil, afterDelay: 0.5)
                
            } else {
                self.hideLoadingView()
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    @objc func getRankingList() {
        
        rankingUsers.removeAll()
        arrGroupMembers.removeAll()
        isBenchMarked = false
        
        ApiRequest.getRankingList(groupId: (selectedGroup.id)) { (resultCode, rankingList) in
            
            if resultCode == Const.CODE_SUCCESS {
                
                rankingUsers = rankingList
                
                var place: Int = 0
                for rankingUser in rankingUsers {
                    place += 1
                    if rankingUser.userId == currentUser!.userId {
                        self.lblCurrentPlace.text = "Current Placing : " + "\(place)"
                    } else {
                        self.arrGroupMembers.append(rankingUser)
                        //TODO: setup groupmembers view height
                        self.vwMemberList.frame.size.height = 70
                        //check if benchmarked user is in group mamber or not
                        if rankingUser.benchmark == 1 {
                            self.isBenchMarked = true
                        }
                    }
                }
                self.getEatBlockTillNow()
                self.tblPointParams.reloadData()
                
            } else {
                self.hideLoadingView()
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func getEatBlockTillNow() {
        
        ApiRequest.getEatBlockTillNow(userId: currentUser!.userId) { (resultCode, eatenBlock) in
            
            self.hideLoadingView()
            
            if resultCode == Const.CODE_SUCCESS {
                self.eatenBlocks = eatenBlock
                currentUser?.macro.eatBlocksTillNow = eatenBlock
                self.setBlockView()
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
        }
    }
    
    func setBlockView() {
        
        if (currentUser!.macro.totalBlocks > 0) {
            let neededBlocks = currentUser!.macro.totalBlocks - eatenBlocks
            let percent = Float(eatenBlocks)/Float(currentUser!.macro.totalBlocks)*100.0
            lblEatenBlockPercent.text = String(format: "%.f", percent) + "% completed"
            
            lblEatenProtein.text = "\(Int(Calculation.protein(Float(eatenBlocks))))"
            lblEatenCarb.text = "\(Int(Calculation.carb(Float(eatenBlocks))))"
            lblEatenFat.text = "\(Int(Calculation.dailyFat(Float(eatenBlocks))))"
            
            lblNeededProtein.text = "\(Int(Calculation.protein(Float(neededBlocks))))"
            lblNeededCarb.text = "\(Int(Calculation.carb(Float(neededBlocks))))"
            lblNeededFat.text = "\(Int(Calculation.dailyFat(Float(neededBlocks))))"
        }
    }

    @IBAction func onMenu(_ sender : Any) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func onShowGroupPicker(_ sender : Any) {
    
        vwGroupSelect.isHidden = false
    }
    
    @IBAction func onSelectGroup(_ sender : Any) {
        vwGroupSelect.isHidden = true
    }
    
    @IBAction func onHitMacroCount(_ sender: Any) {
        
        if selectedGroup.challengeFinished == 1 {
            return
        }
       
        let remainBlocks = currentUser!.macro.totalBlocks - eatenBlocks
        
        if remainBlocks < 1 {
            self.showAlertDialog(title: nil, message: Const.ALERT_FULLBLOCK, positive: Const.OK, negative: nil)
            return
        }
        
        let message = "You need \(remainBlocks) blocks for the day"
        let alert = UIAlertController(title: Const.ALERT_TITLE, message: message, preferredStyle: .alert)
        
        var mytextField : UITextField?
        
        alert.addTextField { (textField) in
            textField.placeholder = "How many block you ate?"
            textField.keyboardType = UIKeyboardType.numberPad
            
            mytextField = textField
        }
        alert.addAction(UIAlertAction(title: Const.CANCEL, style: .default, handler: {(_) in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: Const.SUBMIT, style: .default, handler: { (_) in
            //let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            if let text = mytextField?.text {
                if let blocks = Int(text) {
                    if (blocks > 0 && blocks <= remainBlocks) {
                        
                        self.showLoadingView()
                        
                        ApiRequest.saveEatBlockTillNow(userId: currentUser!.userId, eatenBlock: self.eatenBlocks + blocks, completion: { (resCode) in
                            
                            self.hideLoadingView()
                            
                            if resCode == Const.CODE_SUCCESS {
                                currentUser?.macro.eatBlocksTillNow += blocks
                                self.eatenBlocks += blocks
                                self.setBlockView()
                                
                                print("Selected group type : ", self.selectedGroup.groupType)
                                
                                if remainBlocks == blocks && self.selectedGroup.groupType == arrGroupTypeTitles[1] {
                                    //TODO: Add point
                                    self.showLoadingView()
                                    ApiRequest.addPoint(userId: currentUser!.userId, groupId: self.selectedGroup.id, pointCnt: 5, completion: { (resultCode) in
                                        
                                        if resultCode == Const.CODE_SUCCESS {
                                            self.getRankingList()
                                            self.addPoint(point: 5)
                                        }
                                    })
                                }
                            }
                        })
                    }
                }
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func gotoChatting(_ sender: Any) {
        let chattingVC = ChattingViewController()
        chattingVC._group = selectedGroup        
        self.navigationController?.pushViewController(chattingVC, animated: true)
    }
    
    @IBAction func gotoRanking(_ sender: Any) {        
        
        let rankingVC = self.storyboard?.instantiateViewController(withIdentifier: "RankingViewController") as! RankingViewController
        rankingVC.rankingList = rankingUsers
        rankingVC.selectedGroup = self.selectedGroup
        self.navigationController?.pushViewController(rankingVC, animated: true)
    }
    
    @IBAction func gotoGroupMember(_ sender: Any) {
        
        let groupMemberVC = self.storyboard?.instantiateViewController(withIdentifier: "GroupMembersViewController") as! GroupMembersViewController
        groupMemberVC.memberList = rankingUsers
        groupMemberVC.selectedGroup = selectedGroup
        groupMemberVC.isBenchMarked = isBenchMarked
        groupMemberVC.dismissHandler = {
            // TODO  dismiss handler
            
            if isGranted {
                self.showLoadingView()
                self.getRankingList()                
                isGranted = false
            }
            
        }
        //groupMemberVC.modalPresentationStyle = .overCurrentContext

        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.fluid(percentage: 0.7), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: groupMemberVC, animated: true, completion: nil)
    }

    //MARK:- UITableView Delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrParams.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PointParamsCell") as! PointParamsCell
        
        switch arrParams[indexPath.row] {
        case Const.KEY_PARAM1:
            cell.lblParam.text = arrPointParmams[0]
            checkedParams[0] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM2:
            cell.lblParam.text = arrPointParmams[1]
            checkedParams[1] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM3:
            cell.lblParam.text = arrPointParmams[2]
            checkedParams[2] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM4:
            cell.lblParam.text = arrPointParmams[3]
            checkedParams[3] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM5:
            cell.lblParam.text = arrPointParmams[4]
            checkedParams[4] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM6:
            cell.lblParam.text = arrPointParmams[5]
            self.isBenchMarked ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM7:
            cell.lblParam.text = arrPointParmams[6]
            checkedParams[6] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM8:
            cell.lblParam.text = arrPointParmams[7]
            checkedParams[7] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM9:
            cell.lblParam.text = arrPointParmams[8]
            checkedParams[8] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM10:
            cell.lblParam.text = arrPointParmams[9]
            checkedParams[9] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM11:
            cell.lblParam.text = arrPointParmams[10]
            checkedParams[10] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM12:
            cell.lblParam.text = arrPointParmams[11]
            checkedParams[11] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM_PALEO_BASE:
            cell.lblParam.text = arrPointParmams[12]
            checkedParams[12] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM_KETO_BASE:
            cell.lblParam.text = arrPointParmams[13]
            checkedParams[13] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        case Const.KEY_PARAM_GENERAL_BASE:
            cell.lblParam.text = arrPointParmams[14]
            checkedParams[14] == true ? cell.setCell(status: true) : cell.setCell(status: false)
            break
        default: break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("arrParams==", arrParams[indexPath.row])       
        
        if selectedGroup.challengeFinished == 1 {
            return
        }
        
        if arrParams[indexPath.row] != Const.KEY_PARAM6 {
            
            pointCount = 1
            
            switch arrParams[indexPath.row] {
            case Const.KEY_PARAM1:
                checkedParams[0] = !checkedParams[0]
                updatePointValueSetup(checkedParams[0])
                break
            case Const.KEY_PARAM2:
                checkedParams[1] = !checkedParams[1]
                updatePointValueSetup(checkedParams[1])
                break
            case Const.KEY_PARAM3:
                checkedParams[2] = !checkedParams[2]
                updatePointValueSetup(checkedParams[2])
                break
            case Const.KEY_PARAM4:
                checkedParams[3] = !checkedParams[3]
                updatePointValueSetup(checkedParams[3])
                break
            case Const.KEY_PARAM5:
                checkedParams[4] = !checkedParams[4]
                updatePointValueSetup(checkedParams[4])
                break
            case Const.KEY_PARAM6:
                //checkedParams[5] = true
                break
            case Const.KEY_PARAM7:
                checkedParams[6] = !checkedParams[6]
                updatePointValueSetup(checkedParams[6])
                break
            case Const.KEY_PARAM8:
                checkedParams[7] = !checkedParams[7]
                updatePointValueSetup(checkedParams[7])
                break
            case Const.KEY_PARAM9:
                checkedParams[8] = !checkedParams[8]
                updatePointValueSetup(checkedParams[8])
                break
            case Const.KEY_PARAM10:
                checkedParams[9] = !checkedParams[9]
                updatePointValueSetup(checkedParams[9])
                break
            case Const.KEY_PARAM11:
                checkedParams[10] = !checkedParams[10]
                updatePointValueSetup(checkedParams[10])
                break
            case Const.KEY_PARAM12:
                checkedParams[11] = !checkedParams[11]
                updatePointValueSetup(checkedParams[11])
                break
            case Const.KEY_PARAM_PALEO_BASE:
                pointCount = 5
                checkedParams[12] = !checkedParams[12]
                updatePointValueSetup(checkedParams[12])
                break
            case Const.KEY_PARAM_KETO_BASE:
                pointCount = 5
                checkedParams[13] = !checkedParams[13]
                updatePointValueSetup(checkedParams[13])
                break
            case Const.KEY_PARAM_GENERAL_BASE:
                pointCount = 5
                checkedParams[14] = !checkedParams[14]
                updatePointValueSetup(checkedParams[14])
                break
            default: break
            }
        }
    }
    
    func updatePointValueSetup(_ isChecked: Bool) {
        
        if !isChecked {
            
            self.showLoadingView()
            ApiRequest.deletePoint(userId: currentUser!.userId, groupId: selectedGroup.id, pointCnt: pointCount, completion: { (resultCode) in
                
                if resultCode == Const.CODE_SUCCESS {
                    self.getRankingList()
                    self.deletePoint(point: self.pointCount)
                }
            })
        }
        else {
            
            self.showLoadingView()
            ApiRequest.addPoint(userId: currentUser!.userId, groupId: selectedGroup.id, pointCnt: pointCount, completion: { (resultCode) in
                
                if resultCode == Const.CODE_SUCCESS {
                    self.getRankingList()
                    self.addPoint(point: self.pointCount)
                }
            })
        }
        
        // save changed checked Parameters
        UserDefaults.standard.set(checkedParams, forKey: Const.KEY_CHECKED_PARAMS)
        
        // save checked date
        UserDefaults.standard.set(todayDate, forKey: Const.KEY_SAVEDDATE)
        
        tblPointParams.reloadData()
    
    }
    
    //MARK: - UIPickerView DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrGroups.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrGroups[row].groupName + "(\(arrGroups[row].kindOfGroup))"
    }
    
    @IBAction func onSelect(_ sender: Any) {
        
        selectedGroup = arrGroups[vwGroupPicker.selectedRow(inComponent: 0)]
        vwGroupSelect.isHidden = true
        if selectedGroup.creatorId == currentUser!.userId {
            lblGroupName.text = selectedGroup.groupName + " (My group)"
        } else {
            lblGroupName.text = selectedGroup.groupName
        }
        
        initData()
        self.showLoadingView()
        self.perform(#selector(self.getRankingList), with: nil, afterDelay: 0.5)
    }    

}












