//
//  LibraryViewController.swift
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

import UIKit
import AVKit

class LibraryViewController: BaseViewController, SlideNavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblLibrary: UITableView!
    
    var libraryList = [LibraryModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblLibrary.rowHeight = UITableViewAutomaticDimension
        tblLibrary.estimatedRowHeight = 300
        
        getLibrary()
        
        tblLibrary.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onMenu(_ sender:Any) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    func getLibrary() {
        
        self.showLoadingView()
        
        ApiRequest.getLibrary { (resCode, libraryList) in
            
            self.hideLoadingView()
            
            if resCode == Const.CODE_SUCCESS {
                
                self.libraryList = libraryList
                self.tblLibrary.reloadData()
                
            } else {
                self.showAlertDialog(title: Const.APP_NAME, message: Const.ERROR_CONNECT, positive: Const.OK, negative: nil)
            }
            
        }
    }
    
    // MARK:- tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return libraryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LibraryCell") as! LibraryCell
        
        let lib = libraryList[indexPath.row]
        
        cell.lblTitle.text = lib.title
        cell.imvPicture.sd_setImage(with: URL(string: lib.preview_picture), placeholderImage: #imageLiteral(resourceName: "man"))
        cell.txvDescription.text = lib.description
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //return UITableViewAutomaticDimension
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let fileURL = libraryList[indexPath.row].file_url
        
        if fileURL.range(of:".pdf") != nil || fileURL.range(of:".PDF") != nil {
            let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "LibDetailViewController") as! LibDetailViewController
            detailVC.file_url = fileURL
            self.navigationController?.present(detailVC, animated: true, completion: nil)
        } else {
            let videoURL = URL(string: fileURL)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        
        
    }
    

    
}
