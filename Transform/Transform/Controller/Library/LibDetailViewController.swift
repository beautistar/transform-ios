//
//  LibDetailViewController.swift
//  Transform
//
//  Created by Yin on 2018/5/20.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit


class LibDetailViewController: UIViewController {
    
    var file_url = ""

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        webView.loadRequest(URLRequest(url: URL(string: file_url)!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    

    

}
