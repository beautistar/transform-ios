//
//  Objective-CBridgingHeader.h
//  Transform
//
//  Created by Yin on 09/11/2017.
//  Copyright © 2017 Yin. All rights reserved.
//

#ifndef Objective_CBridgingHeader_h
#define Objective_CBridgingHeader_h

#import "SlideNavigationController.h"
#import "UIImage+ResizeMagick.h"
#import "XlsxReaderWriter-swift-bridge.h"
//#import "AcceptSDK.h"
#endif /* Objective_CBridgingHeader_h */
